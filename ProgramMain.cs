﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using se_unit_tests.UnitTests;

namespace se_unit_tests
{
    class ProgramMain
    {
        static void Main(string[] args)
        {
            var unitTestMain = new UnitTestMain();
            unitTestMain.RunTests();
        }
    }
}
