﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace se_unit_tests.UnitTests
{

    public struct PolynomialRoots : IEnumerable<ComplexNumber>
    {
        const double Epsilon = 1e-12;
        ComplexNumber _root0;
        ComplexNumber _root1;
        ComplexNumber _root2;
        ComplexNumber _root3;

        int _writeIdx;

        public PolynomialRoots(byte _ = 0)
        {
            _root0 = _root1 = _root2 = _root3 = ComplexNumber.Zero;
            _writeIdx = 0;
        }

        public bool IsFull => _writeIdx == 4;

        public int Count => _writeIdx;

        public ComplexNumber this[int pos]
        {
            get
            {
                switch (pos)
                {
                    case 0: return _root0;
                    case 1: return _root1;
                    case 2: return _root2;
                    case 3: return _root3;
                    default: throw new ArgumentOutOfRangeException(nameof(pos));
                }
            }
            set
            {
                switch (pos)
                {
                    case 0: _root0 = value; break;
                    case 1: _root1 = value; break;
                    case 2: _root2 = value; break;
                    case 3: _root3 = value; break;
                    default: throw new ArgumentOutOfRangeException(nameof(pos));
                }
            }
        }

        public bool AddRoot(ComplexNumber s)
        {
            if (IsFull)
            {
                return false;
            }

            for (int pos = 0; pos < _writeIdx; ++pos)
            {
                if ((s - this[pos]).MagnitudeSquared() < Epsilon)
                {
                    return false;
                }
            }

            this[_writeIdx++] = s;
            return true;
        }

        public IEnumerator<ComplexNumber> GetEnumerator()
        {
            for (int ii = 0; ii < Count; ++ii)
            {
                yield return this[ii];
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return (IEnumerator)GetEnumerator();
        }
    }
}
