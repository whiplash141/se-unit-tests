﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace se_unit_tests.UnitTests
{
    public static class Quartic
    {
        const double Epsilon = 1e-12;


        /// <summary>
        /// Finds the roots of a quartic function in the form: a*x^4 + b*x^3 + c*x^2 + d*x + e = 0.
        /// </summary>
        /// <see href="https://mathworld.wolfram.com/QuarticFormula.html"/>
        /// <returns>True if solution exists</returns>
        public static bool FindRoots(double a, double b, double c, double d, double e, out PolynomialRoots roots, double epsilon = Epsilon)
        {
            roots = new PolynomialRoots();

            if (Math.Abs(a) < epsilon)
            {
                PolynomialRoots x;
                if (!Cubic.FindRoots(b,c,d,e, out x, epsilon))
                {
                    return false;
                }

                foreach (ComplexNumber root in x)
                {
                    roots.AddRoot(root);
                }
                return true;
            }

            double invA = 1.0 / a;
            double a3 = b*invA;
            double a2 = c * invA;
            double a1 = d * invA;
            double a0 = e * invA;

            // Resolvent cubic
            PolynomialRoots ySoln;
            if (!Cubic.FindRoots(1, -a2, a1*a3 - 4*a0, 4*a2*a0 - a1*a1 - a3*a3*a0, out ySoln, epsilon))
            {
                return false;
            }

            double? y1 = null;
            foreach(ComplexNumber y in ySoln)
            {
                if (y.IsReal)
                {
                    y1 = y.Real;
                    break;
                }
            }
            if (!y1.HasValue)
            {
                return false;
            }

            ComplexNumber R = ComplexNumber.PrincipalRoot(0.25 * a3 * a3 - a2 + y1.Value, 2);
            if (!R.IsReal)
            {
                return false;
            }

            ComplexNumber D, E;
            if (Math.Abs(R.Real) > epsilon)
            {
                ComplexNumber RInv = 1.0 / R;
                ComplexNumber k1 = 0.75 * a3 * a3 - R * R - 2 * a2;
                ComplexNumber k2 = 0.25 * (4 * a3 * a2 - 8 * a1 - a3 * a3 * a3) * RInv;
                D = ComplexNumber.PrincipalRoot(k1 + k2, 2);
                E = ComplexNumber.PrincipalRoot(k1 - k2, 2);
            }
            else
            {
                double k1 = 0.75 * a3 * a3 - 2 * a2;
                ComplexNumber k2 = 2 * ComplexNumber.PrincipalRoot(y1.Value * y1.Value - 4 * a0, 2);
                D = ComplexNumber.PrincipalRoot(k1 + k2, 2);
                E = ComplexNumber.PrincipalRoot(k1 - k2, 2);
            }

            roots.AddRoot(-0.25*a3 + 0.5*R + 0.5*D);
            roots.AddRoot(-0.25*a3 + 0.5*R - 0.5*D);
            roots.AddRoot(-0.25*a3 - 0.5*R + 0.5*E);
            roots.AddRoot(-0.25*a3 - 0.5*R - 0.5*E);

            return true;
        }
    }
}
