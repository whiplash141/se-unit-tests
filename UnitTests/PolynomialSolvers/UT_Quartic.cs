﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace se_unit_tests.UnitTests
{
    class UT_Quartic : UT_Base
    {
        public UT_Quartic()
        {
            base.Name = "Quartic";
        }

        struct Expected
        {
            public double Re;
            public double Im;

            public Expected(double re, double im)
            {
                Re = re;
                Im = im;
            }
        }

        public override void Run()
        {
            {
                PolynomialRoots roots;
                bool hasSolution = Quartic.FindRoots(1, 7, -19, -7, 18, out roots);

                Assert.IsTrue("Has solution", hasSolution);
                Assert.IsEqual("Solution count", roots.Count, 4);

                Expected[] expected = new Expected[]
                {
                    new Expected(-1,0),
                    new Expected(1,0),
                    new Expected(2,0),
                    new Expected(-9,0)
                };

                double eps = 1e-12;
                for (int ii = 0; ii < expected.Length; ++ii)
                {
                    var exp = expected[ii];
                    bool found = false;
                    foreach (ComplexNumber r in roots)
                    {
                        if (Math.Abs(exp.Re - r.Real) < eps && Math.Abs(exp.Im - r.Imaginary) < eps)
                        {
                            found = true;
                            break;
                        }
                    }
                    Assert.IsTrue($"Found solution {ii + 1}", found);
                }
            }
            {
                PolynomialRoots roots;
                bool hasSolution = Quartic.FindRoots(1, -4, 3, 4, -4, out roots);

                Assert.IsTrue("Has solution", hasSolution);
                Assert.IsEqual("Solution count", roots.Count, 3);

                Expected[] expected = new Expected[]
                {
                    new Expected(-1,0),
                    new Expected(1,0),
                    new Expected(2,0),
                };

                double eps = 1e-12;
                for (int ii = 0; ii < expected.Length; ++ii)
                {
                    var exp = expected[ii];
                    bool found = false;
                    foreach (ComplexNumber r in roots)
                    {
                        if (Math.Abs(exp.Re - r.Real) < eps && Math.Abs(exp.Im - r.Imaginary) < eps)
                        {
                            found = true;
                            break;
                        }
                    }
                    Assert.IsTrue($"Found solution {ii + 1}", found);
                }
            }
        }
    }
}
