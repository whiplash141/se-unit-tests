﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Sandbox.Game.Weapons.MyDrillBase;

namespace se_unit_tests.UnitTests
{
    class UT_Cubic : UT_Base
    {
        public UT_Cubic()
        {
            base.Name = "Cubic";
        }

        struct Expected
        {
            public double Re;
            public double Im;

            public Expected(double re, double im)
            {
                Re = re;
                Im = im;
            }
        }

        public override void Run()
        {
            PolynomialRoots solution;
            // Expected roots: -1,1,2
            bool hasSolution = Cubic.FindRoots(1, -2, -1, 2, out solution);
            Assert.IsTrue("Solutions found", hasSolution);
            Assert.IsEqual("Number of solututions", solution.Count, 3);

            Expected[] expected = new Expected[3]
            {
                new Expected(-1,0),
                new Expected(1,0),
                new Expected(2,0)
            };

            double eps = 1e-12;
            for (int ii = 0; ii < expected.Length; ++ii)
            {
                var exp = expected[ii];
                bool found = false;
                foreach (var soln in solution)
                {
                    if (Math.Abs(exp.Re - soln.Real) < eps && Math.Abs(exp.Im - soln.Imaginary) < eps)
                    {
                        found = true; 
                        break;
                    }
                }
                Assert.IsTrue($"Found solution {ii + 1}", found);
            }
        }
    }
}
