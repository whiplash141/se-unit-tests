﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace se_unit_tests.UnitTests
{
    public static class Quadratic
    {
        const double Epsilon = 1e-12;

        /// <summary>
        /// Finds roots of a quadratic equation in the form: a*x^2 + b*x + c = 0.
        /// </summary>
        /// <returns>True if a solution exists</returns>
        public static bool FindRoots(double a, double b, double c, out PolynomialRoots roots, double epsilon = Epsilon)
        {
            roots = new PolynomialRoots();

            // Linear
            if (Math.Abs(a) < epsilon)
            {
                if (Math.Abs(b) < epsilon)
                {
                    return false;
                }
                var x = new ComplexNumber(-c / b, 0);
                roots.AddRoot(x);
                return true;
            }

            // Quadratic
            double d = b * b - 4.0 * a * c;
            double inv2a = 1.0 / (2.0 * a);
            for (uint root = 0; root < 2; ++root)
            {
                roots.AddRoot((-b + ComplexNumber.Sqrt(d, root)) * inv2a);
            }
            return true;
        }

        /// <summary>
        /// Solves a quadratic in the form: 0 = a*x^2 + b*x + c.
        /// If only one solution exists, xMin = xMax.
        /// </summary>
        /// <returns>True if a real solution exists.</returns>
        public static bool FindRoots(double a, double b, double c, out double xMax, out double xMin, double epsilon = Epsilon)
        {
            xMin = xMax = 0;

            PolynomialRoots x;
            if (!FindRoots(a, b, c, out x, epsilon))
            {
                return false;
            }

            if (x[0].IsReal && x[1].IsReal)
            {
                xMin = Math.Min(x[0].Real, x[1].Real);
                xMax = Math.Max(x[0].Real, x[1].Real);
            }
            else
            {
                return false;
            }
            return true;
        }
    }
}
