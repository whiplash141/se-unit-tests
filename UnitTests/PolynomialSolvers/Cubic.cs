﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace se_unit_tests.UnitTests
{
    public static class Cubic
    {
        const double Epsilon = 1e-12;


        /// <summary>
        /// Finds the roots of a cubic function in the form: a*x^3 + b*x^2 + c*x + d = 0.
        /// </summary>
        /// <see href="https://mathworld.wolfram.com/CubicFormula.html"/>
        /// <returns>True if solution exists</returns>
        public static bool FindRoots(double a, double b, double c, double d, out PolynomialRoots roots, double epsilon = Epsilon)
        {
            roots = new PolynomialRoots();
            if (Math.Abs(a) < 1e-9)
            {
                PolynomialRoots xSoln;
                if (!Quadratic.FindRoots(b,c,d, out xSoln, epsilon))
                {
                    return false;
                }

                foreach (ComplexNumber x in xSoln)
                {
                    roots.AddRoot(x);
                }
                return true;
            }

            double invA = 1.0 / a;
            double a2 = b * invA;
            double a1 = c * invA;
            double a0 = d * invA;

            double p = (3 * a1 - a2 * a2) / 3;
            double q = (9 * a1 * a2 - 27 * a0 - 2 * a2 * a2 * a2) / 27;

            PolynomialRoots uSoln;
            if (!Quadratic.FindRoots(1, -q, -1.0 / 27.0 * p * p * p, out uSoln, epsilon))
            {
                return false;
            }

            for (uint cubeIdx = 0; cubeIdx < 3; ++cubeIdx)
            {
                foreach (ComplexNumber u in uSoln)
                {
                    if (roots.IsFull)
                    {
                        break;
                    }
                    var w = ComplexNumber.Root(u, 3, cubeIdx);
                    ComplexNumber x = w - p / (3 * w) - 1.0 / 3.0 * a2;
                    roots.AddRoot(x);
                }
            }

            return true;
        }
    }
}
