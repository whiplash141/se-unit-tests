﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace se_unit_tests.UnitTests.Dummies
{
    public class Program
    {
        // Dummy class
        public Runtime Runtime = new Runtime();
        public IMyProgrammableBlock Me = new IMyProgrammableBlock();
    }
}
