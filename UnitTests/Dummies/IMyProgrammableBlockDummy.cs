﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VRageMath;

namespace se_unit_tests.UnitTests.Dummies
{
    public class IMyProgrammableBlock
    {
        Vector3D _pos;
        public Vector3D GetPosition()
        {
            return _pos;
        }

        public void DUMMY_SetPosition(Vector3D v)
        {
            _pos = v;
        }
    }
}
