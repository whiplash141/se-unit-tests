﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VRageMath;

namespace se_unit_tests.UnitTests
{
    static class TaylorSeriesExpansions
    {
        const double TwoPi = Math.PI * 2;
        const double InvFact2 = 1d / 2d;
        const double InvFact4 = 1d / 24d;
        const double InvFact6 = 1d / 720d;
        const double InvFact8 = 1d / 40320d;
        const double InvFact10 = 1d / 362880d;

        public static double Sin(double angle, int numTerms)
        {
            angle -= Math.PI / 2;
            angle %= TwoPi;
            if (angle > Math.PI)
            {
                angle = angle - TwoPi;
            }
            else if (angle < -Math.PI)
            {
                angle = angle + TwoPi;
            }
            return Cos(angle, numTerms);
        }

        public static double Cos(double angle, int numTerms)
        {
            // Angle from -PI to PI
            angle %= TwoPi;
            if (angle > Math.PI)
            {
                angle = angle - TwoPi;
            }
            else if (angle < -Math.PI)
            {
                angle = angle + TwoPi;
            }

            double num = angle;
            int finalSign = 1;
            if (angle > Math.PI / 2)
            {
                num = Math.PI - angle;
                finalSign = -1;
            }
            else if (angle < -Math.PI / 2)
            {
                num = -Math.PI - angle;
                finalSign = -1;
            }
            
            double sum = 0;
            for (int i = 0; i < numTerms; ++i)
            {
                int sign = i % 2 == 0 ? 1 : -1;
                sum += sign * Math.Pow(num, 2 * i) / Factorial(2 * i);
            }
            return finalSign * sum;
        }

        public static double Sin(double angle)
        {
            angle -= Math.PI / 2;
            return Cos(angle);
        }

        public static double Cos(double angle)
        {
            // Angle from -PI to PI
            angle = MathHelper.WrapAngle((float)angle);
            /*
            angle %= TwoPi;
            if (angle > Math.PI)
            {
                angle = angle - TwoPi;
            }
            else if (angle < -Math.PI)
            {
                angle = angle + TwoPi;
            }
            */

            // Take advantage of vertical symmetry of cosine
            // to keep taylor series approx. closer to x = 0
            double x = angle;
            int sign = 1;
            if (angle > Math.PI / 2)
            {
                x = Math.PI - angle;
                sign = -1;
            }
            else if (angle < -Math.PI / 2)
            {
                x = -Math.PI - angle;
                sign = -1;
            }

            double x2 = x * x;
            double x4 = x2 * x2;
            double x6 = x4 * x2;
            double x8 = x6 * x2;
            double x10 = x8 * x2;
            return sign * (1 - x2 * InvFact2 + x4 * InvFact4 - x6 * InvFact6 + x8 * InvFact8 - x10 * InvFact10);
        }

        public static double Acos(double num)
        {
            if (num < -1 || num > 1)
            {
                return double.NaN;
            }
            double x = num;
            double x2 = x * x;
            double x3 = x * x2;
            double x5 = x3 * x2;
            double x7 = x5 * x2;

            return 1d / 2d * Math.PI - x - 1d / 6d * x3 - 3d / 40d * x5 - 5d / 112d * x7 - 35d / 1152d * x7 * x2;
        }

        static long Factorial(long num)
        {
            if (num <= 1)
                return 1;
            return num * Factorial(num - 1);
        }
    }
}
