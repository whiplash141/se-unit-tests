﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace se_unit_tests.UnitTests
{
    class UT_TaylorSeriesExpansions : UT_Base
    {
        public UT_TaylorSeriesExpansions()
        {
            this.Name = "TaylorSeriesExpansions";
        }

        public override void Run()
        {

            Random rng = new Random();
            double epsilon = 1e-3;
            double[] testAngles = new double[]
            {
                0,
                Math.PI * 1 / 3,
                Math.PI * 2 / 3,
                Math.PI * 3 / 3,
                Math.PI * 4 / 3,
                Math.PI * 5 / 3,
                Math.PI * 6 / 3,
                Math.PI * 7 / 3,
                Math.PI * 8 / 3,
                Math.PI * 9 / 3,
                -Math.PI * 1 / 3,
                -Math.PI * 2 / 3,
                -Math.PI * 3 / 3,
                -Math.PI * 4 / 3,
                -Math.PI * 5 / 3,
                -Math.PI * 6 / 3,
                -Math.PI * 7 / 3,
                -Math.PI * 8 / 3,
                -Math.PI * 9 / 3,
            };

            double[] testNums = new double[]
            {
                -1.0,
                -0.9,
                -0.8,
                -0.7,
                -0.6,
                -0.5,
                -0.4,
                -0.3,
                -0.2,
                -0.1,
                0.0,
                0.1,
                0.2,
                0.3,
                0.4,
                0.5,
                0.6,
                0.7,
                0.8,
                0.9,
                1.0,
            };

            foreach (var angle in testAngles)
            {
                double sin = Math.Sin(angle);
                double sinF = TaylorSeriesExpansions.Sin(angle);
                Assert.IsEqual($"Math.Sin({angle:n2}) == TaylorSeriesExpansions.Sin({angle:n2})", sin, sinF, epsilon);
            }

            foreach (var angle in testAngles)
            {
                double cos = Math.Sin(angle);
                double cosF = TaylorSeriesExpansions.Sin(angle);
                Assert.IsEqual($"Math.Cos({angle:n2}) == TaylorSeriesExpansions.Cos({angle:n2})", cos, cosF, epsilon);
            }

            foreach(var num in testNums)
            {
                double angle = Math.Acos(num);
                double angleF = TaylorSeriesExpansions.Acos(num);
                Assert.IsEqual($"Math.Acos({num:n2}) == TaylorSeriesExpansions.Acos({num:n2})", angle, angleF, epsilon);
            }
        }
    }
}
