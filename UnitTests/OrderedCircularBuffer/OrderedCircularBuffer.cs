﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace se_unit_tests.UnitTests
{

    /// <summary>
    /// An ordered, generic circular buffer class with a fixed capacity.
    /// The newest item is always the first element in the buffer and the oldest is always the last.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class OrderedCircularBuffer<T>
    {
        public readonly int Capacity;

        readonly T[] _array = null;

        private int _indexOffset = 0;

        /// <summary>
        /// Array accessor operator.
        /// </summary>
        /// <param name="index"></param>
        public T this[int index]
        {
            get
            {
                return _array[(index + _indexOffset) % Capacity];
            }
            set
            {
                _array[(index + _indexOffset) % Capacity] = value;
            }
        }

        /// <summary>
        /// OrderedCircularBuffer ctor.
        /// </summary>
        /// <param name="capacity">Capacity of the CircularBuffer.</param>
        public OrderedCircularBuffer(int capacity)
        {
            if (capacity < 2)
                throw new Exception($"Capacity of CircularBuffer ({capacity}) can not be less than 2");
            Capacity = capacity;
            _array = new T[Capacity];
        }

        /// <summary>
        /// Adds an item to the buffer. If the buffer is full, it will overwrite the oldest value.
        /// </summary>
        /// <param name="item"></param>
        public void Add(T item)
        {
            --_indexOffset;
            if (_indexOffset < 0)
            {
                _indexOffset = Capacity - 1;
            }
            _array[_indexOffset] = item;
        }
    }
}
