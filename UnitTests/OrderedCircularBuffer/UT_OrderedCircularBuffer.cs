﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace se_unit_tests.UnitTests
{
    class UT_OrderedCircularBuffer : UT_Base
    {
        public UT_OrderedCircularBuffer()
        {
            base.Name = "OrderedCircularBuffer";
        }

        public override void Run()
        {

            #region Capacity validation
            {
                bool caught = false;
                try
                {
                    OrderedCircularBuffer<int> badBuffer = new OrderedCircularBuffer<int>(0);
                    caught = false;
                }
                catch
                {
                    caught = true;
                }
                Assert.IsTrue("Threw an exception for capacity less than 1", caught);
            }
            #endregion

            #region Write Insertion
            {
                OrderedCircularBuffer<int> circularBuffer = new OrderedCircularBuffer<int>(3);
                circularBuffer.Add(3);
                circularBuffer.Add(2);
                circularBuffer.Add(1);
                {
                    int[] expectedArr = new int[] { 1, 2, 3};
                    AssertIsExpected(circularBuffer, expectedArr, "Buffer properly inserts new values (1)");
                }
                circularBuffer.Add(4);
                {
                    int[] expectedArr = new int[] { 4, 1, 2 };
                    AssertIsExpected(circularBuffer, expectedArr, "Buffer properly inserts new values (2)");
                }
                circularBuffer.Add(4);
                {
                    int[] expectedArr = new int[] { 4, 4, 1 };
                    AssertIsExpected(circularBuffer, expectedArr, "Buffer properly inserts new values (3)");
                }
            }
            #endregion

        }

        public void AssertIsExpected<T>(OrderedCircularBuffer<T> circularBuffer, T[] expectedArr, string message)
        {
            bool failed = false;
            for (int i = 0; i < expectedArr.Length; ++i)
            {
                T val = circularBuffer[i];
                failed |= val.Equals(expectedArr[i]) == false;
            }
            Assert.IsFalse(message, failed);
        }
    }
}
