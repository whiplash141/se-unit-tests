﻿using System;
using VRageMath;

namespace se_unit_tests.UnitTests
{
    public static class VectorFromAzimuthAndElevationContainer
    {
        public static void VectorFromAzimuthAndElevation(double azimuth, double elevation, out Vector3D direction)
        {
            double sinAz = MyMath.FastSin((float)azimuth);
            double cosAz = MyMath.FastCos((float)azimuth);

            double sinEl = MyMath.FastSin((float)elevation);
            double cosEl = MyMath.FastCos((float)elevation);

            MatrixD compositeRotation = MatrixD.Zero;
            compositeRotation.M11 = cosAz;
            compositeRotation.M12 = sinAz * sinEl;
            compositeRotation.M13 = -sinAz * cosEl;
            compositeRotation.M21 = 0;
            compositeRotation.M22 = cosEl;
            compositeRotation.M23 = sinEl;
            compositeRotation.M31 = sinAz;
            compositeRotation.M32 = -sinEl * cosAz;
            compositeRotation.M33 = cosAz * cosEl;

            direction = Vector3D.Forward;
            Vector3D.Rotate(ref direction, ref compositeRotation, out direction);
        }
    }
}
