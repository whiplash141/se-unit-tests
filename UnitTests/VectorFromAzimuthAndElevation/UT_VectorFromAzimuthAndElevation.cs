﻿using System;
using VRageMath;

namespace se_unit_tests.UnitTests
{
    class UT_VectorFromAzimuthAndElevation : UT_Base
    {
        public UT_VectorFromAzimuthAndElevation()
        {
            this.Name = "VectorFromAzimuthAndElevation";
        }

        public override void Run()
        {
            MyMath.InitializeFastSin();

            double azimuth = MathHelperD.ToRadians(45);
            double elevation = MathHelperD.ToRadians(-30);
            Vector3D expected = new Vector3D(-0.707106781186547, -0.353553390593274, -0.612372435695795);
            Vector3D result;
            VectorFromAzimuthAndElevationContainer.VectorFromAzimuthAndElevation(azimuth, elevation, out result);
            Assert.IsEqual("Nominal test", expected, result, 1e-3);

            azimuth = MathHelperD.ToRadians(720);
            elevation = MathHelperD.ToRadians(-360);
            expected = new Vector3D(0,0,-1);
            VectorFromAzimuthAndElevationContainer.VectorFromAzimuthAndElevation(azimuth, elevation, out result);
            Assert.IsEqual("Test wrap around", expected, result, 1e-3);

        }
    }
}
