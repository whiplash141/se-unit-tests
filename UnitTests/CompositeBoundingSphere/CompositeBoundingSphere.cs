﻿using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using VRageMath;
using VRage.Game;
using Sandbox.ModAPI.Interfaces;
using Sandbox.ModAPI.Ingame;
using Sandbox.Game.EntityComponents;
using VRage.Game.Components;
using VRage.Collections;
using VRage.Game.ObjectBuilders.Definitions;
using VRage.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame.Utilities;
using SpaceEngineers.Game.ModAPI.Ingame;
using VRage;
using System.Linq;
using VRage.Game.GUI.TextPanel;
using se_unit_tests.UnitTests.Dummies;


namespace se_unit_tests.UnitTests
{
    public class DUMMY_IMyProgrammableBlock
    {
        public Vector3D DUMMY_Position;
        public Vector3D GetPosition()
        {
            return DUMMY_Position;
        }

        public MatrixD WorldMatrix
        {
            get
            {
                return MatrixD.CreateRotationZ(Math.PI);
            }
        }

        public DUMMY_IMyProgrammableBlock(Vector3D pos)
        {
            DUMMY_Position = pos;
        }
    }

    public class DUMMY_IMyCubeGrid
    {
        Vector3D _position;
        double _radius;
        public DUMMY_IMyCubeGrid(Vector3D pos, double rad)
        {
            _position = pos;
            _radius = rad;
        }

        public BoundingSphereD WorldVolume
        {
            get
            {
                return new BoundingSphereD(_position, _radius);
            }
        }
    }

    public class CompositeBoundingSphere
    {
        public double Radius
        {
            get
            {
                return _sphere.Radius;
            }
        }

        public Vector3D Center
        {
            get
            {
                return _sphere.Center;
            }
        }

        BoundingSphereD _sphere;

        DUMMY_IMyProgrammableBlock PB = new DUMMY_IMyProgrammableBlock(new Vector3D(3, 2, 0));
        Program _program;
        HashSet<DUMMY_IMyCubeGrid> _grids = new HashSet<DUMMY_IMyCubeGrid>()
        {
            new DUMMY_IMyCubeGrid(new Vector3D(-2,0,0), 4),
            new DUMMY_IMyCubeGrid(new Vector3D(3,0,0), 2),
        };
        Vector3D _compositePosLocal = Vector3D.Zero;
        double _compositeRadius = 0;

        public CompositeBoundingSphere(Program program)
        {
            _program = program;
        }

        public void FetchCubeGrids()
        {
            //_grids.Clear();
            //_program.GridTerminalSystem.GetBlocksOfType<IMyMechanicalConnectionBlock>(null, CollectGrids);
            //RecomputeCompositeProperties();
        }

        public void Compute(bool fullCompute = false)
        {
            if (fullCompute)
            {
                RecomputeCompositeProperties();
            }
            Vector3D compositePosWorld = PB.GetPosition() /*_program.Me.GetPosition()*/ + Vector3D.TransformNormal(_compositePosLocal, PB.WorldMatrix);//_program.Me.WorldMatrix);
            _sphere = new BoundingSphereD(compositePosWorld, _compositeRadius);
        }

        void RecomputeCompositeProperties()
        {
            bool first = true;
            Vector3D compositeCenter = Vector3D.Zero;
            double compositeRadius = 0;
            foreach (var g in _grids)
            {
                Vector3D currentCenter = g.WorldVolume.Center;
                double currentRadius = g.WorldVolume.Radius;
                if (first)
                {
                    compositeCenter = currentCenter;
                    compositeRadius = currentRadius;
                    first = false;
                    continue;
                }
                Vector3D diff = currentCenter - compositeCenter;
                double diffLen = diff.Normalize();
                double newDiameter = currentRadius + diffLen + compositeRadius;
                double newRadius = 0.5 * newDiameter;
                if (newRadius > compositeRadius)
                {
                    double diffScale = (newRadius - compositeRadius);
                    compositeRadius = newRadius;
                    compositeCenter += diffScale * diff;
                }
            }
            // Convert to local space
            Vector3D directionToCompositeCenter = compositeCenter - PB.GetPosition();//_program.Me.GetPosition();
            _compositePosLocal = Vector3D.TransformNormal(directionToCompositeCenter, MatrixD.Transpose(PB.WorldMatrix));//_program.Me.WorldMatrix));
            _compositeRadius = compositeRadius;
        }

        /*
        bool CollectGrids(IMyTerminalBlock b)
        {
            var mech = (IMyMechanicalConnectionBlock)b;
            _grids.Add(mech.CubeGrid);
            if (mech.IsAttached)
            {
                _grids.Add(mech.TopGrid);
            }
            return false;
        }
        */
    }
}
