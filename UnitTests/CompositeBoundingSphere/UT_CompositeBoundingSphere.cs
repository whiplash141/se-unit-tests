﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VRageMath;
using se_unit_tests.UnitTests.Dummies;

namespace se_unit_tests.UnitTests
{
    class UT_CompositeBoundingSphere : UT_Base
    {
        public UT_CompositeBoundingSphere()
        {
            base.Name = "CompositeBoundingSphere";
        }

        Program _programDummy = new Program();


        public override void Run()
        {

            var compositeBoundingSphere = new CompositeBoundingSphere(_programDummy);
            compositeBoundingSphere.Compute(true);
            Assert.IsEqual("Composite center check", new Vector3D(-0.5, 0, 0), compositeBoundingSphere.Center);
            Assert.IsEqual("Composite radius check", 5.5, compositeBoundingSphere.Radius);

        }

        public void AssertIsExpected<T>(CircularBuffer<T> circularBuffer, T[] expectedArr, string message)
        {
            bool failed = false;
            for (int i = 0; i < expectedArr.Length; ++i)
            {
                T val = circularBuffer.MoveNext();
                failed |= val.Equals(expectedArr[i]) == false;
            }
            Assert.IsFalse(message, failed);
        }
    }
}
