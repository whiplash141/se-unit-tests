﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using se_unit_tests.UnitTests;
using VRageMath;

namespace se_unit_tests.UnitTests
{
    /// <summary>
    /// Unit tests for vector math class.
    /// </summary>
    class UT_VectorMath : UT_Base
    {
        public UT_VectorMath()
        {
            this.Name = "VectorMath";
        }

        public override void Run()
        {
            Vector3D result, expected, vec1, vec2;
            double val;

            #region SafeNormalize
            // Test zero vector
            vec1 = Vector3D.Zero;
            result = VectorMath.SafeNormalize(vec1);
            Assert.IsEqual("SafeNormalize zero vector components", result, Vector3D.Zero);

            // Test unit vector
            vec1 = new Vector3D(1, 0, 0);
            result = VectorMath.SafeNormalize(vec1);
            Assert.IsEqual("SafeNormalize unit vector length", result.Length(), 1);
            Assert.IsEqual("SafeNormalize unit vector components", result, new Vector3D(1, 0, 0));

            // Test non unit vector
            vec1 = new Vector3D(3, 4, 0);
            result = VectorMath.SafeNormalize(vec1);
            expected = new Vector3D(3 / Math.Sqrt(3 * 3 + 4 * 4), 4 / Math.Sqrt(3 * 3 + 4 * 4), 0);
            Assert.IsEqual("SafeNormalize non-unit vector length", result.Length(), 1);
            Assert.IsEqual("SafeNormalize non-unit vector components", result, expected);
            #endregion

            #region Projection
            // Zero first vector
            vec1 = new Vector3D(0, 0, 0);
            vec2 = new Vector3D(1, 2, 3);
            result = VectorMath.Projection(vec1, vec2);
            Assert.IsEqual("Projection Zero first vector returns zero", result, Vector3D.Zero);

            // Zero second vector
            vec1 = new Vector3D(1, 2, 3);
            vec2 = new Vector3D(0, 0, 0); ;
            result = VectorMath.Projection(vec1, vec2);
            Assert.IsEqual("Projection Zero second vector returns zero", result, Vector3D.Zero);

            // Unit vector
            vec1 = new Vector3D(1, 2, 3);
            vec2 = new Vector3D(0, 1, 0);
            result = VectorMath.Projection(vec1, vec2);
            Assert.IsEqual("Projection Unit second vector", result, new Vector3D(0, 2, 0));

            // Regular
            vec1 = new Vector3D(1, 2, 3);
            vec2 = new Vector3D(0, 10, 0);
            result = VectorMath.Projection(vec1, vec2);
            Assert.IsEqual("Projection Non-unit second vector", result, new Vector3D(0, 2, 0));
            #endregion

            #region Rejection
            // Zero first vector
            vec1 = new Vector3D(0, 0, 0);
            vec2 = new Vector3D(1, 2, 3);
            result = VectorMath.Rejection(vec1, vec2);
            Assert.IsEqual("Rejection Zero first vector returns zero", result, Vector3D.Zero);

            // Zero second vector
            vec1 = new Vector3D(1, 2, 3);
            vec2 = new Vector3D(0, 0, 0); ;
            result = VectorMath.Rejection(vec1, vec2);
            Assert.IsEqual("Rejection Zero second vector returns zero", result, Vector3D.Zero);

            // Unit vector
            vec1 = new Vector3D(1, 2, 3);
            vec2 = new Vector3D(0, 1, 0);
            result = VectorMath.Rejection(vec1, vec2);
            var res2 = Vector3D.Reject(vec1, vec2);
            Assert.IsEqual("Rejection Unit second vector", result, new Vector3D(1, 0, 3));

            // Regular
            vec1 = new Vector3D(1, 2, 3);
            vec2 = new Vector3D(0, 10, 0);
            result = VectorMath.Rejection(vec1, vec2);
            res2 = Vector3D.Reject(vec1, vec2);
            Assert.IsEqual("Rejection Non-unit second vector", result, new Vector3D(1, 0, 3));
            #endregion

            #region Reflection
            // Regular
            vec1 = new Vector3D(1, 2, 3);
            vec2 = new Vector3D(10, 0, 0);
            result = VectorMath.Reflection(vec1, vec2);
            Assert.IsEqual("Reflection Nominal", result, new Vector3D(1, -2, -3));

            // Zero reflection axis
            // Rationale: Reflecting about a point is just a negation
            vec1 = new Vector3D(1, 2, 3);
            vec2 = new Vector3D(0, 0, 0);
            result = VectorMath.Reflection(vec1, vec2);
            Assert.IsEqual("Reflection Zero reflection axis", result, new Vector3D(-1, -2, -3));
            #endregion

            #region ScalarProjection
            vec1 = new Vector3D(0, 0, 0);
            vec2 = new Vector3D(1, 2, 3);
            val = VectorMath.ScalarProjection(vec1, vec2);
            Assert.IsEqual("ScalarProjection Zero first vector", val, 0);

            vec1 = new Vector3D(1, 2, 3);
            vec2 = new Vector3D(0, 0, 0);
            val = VectorMath.ScalarProjection(vec1, vec2);
            Assert.IsEqual("ScalarProjection Zero second vector", val, 0);

            vec1 = new Vector3D(1, 2, 3);
            vec2 = new Vector3D(0, 10, 0);
            val = VectorMath.ScalarProjection(vec1, vec2);
            Assert.IsEqual("ScalarProjection Nominal non-unit second vector", val, 2);
            #endregion

            #region CosBetween
            // Nominal
            vec1 = new Vector3D(1, 2, 0);
            vec2 = new Vector3D(1, 0, 0);
            val = VectorMath.CosBetween(vec1, vec2);
            Assert.IsEqual("CosBetween Nominal", val, 1 / Math.Sqrt(5));

            // Zero first vector
            vec1 = new Vector3D(0, 0, 0);
            vec2 = new Vector3D(1, 0, 0);
            val = VectorMath.CosBetween(vec1, vec2);
            Assert.IsEqual("CosBetween Zero first vector", val, 0);

            // Zero second vector
            vec1 = new Vector3D(1, 0, 0);
            vec2 = new Vector3D(0, 0, 0);
            val = VectorMath.CosBetween(vec1, vec2);
            Assert.IsEqual("CosBetween Zero second vector", val, 0);
            #endregion

            #region AngleBetween
            // Nominal
            vec1 = new Vector3D(1, 2, 0);
            vec2 = new Vector3D(1, 0, 0);
            val = VectorMath.AngleBetween(vec1, vec2);
            Assert.IsEqual("AngleBetween Nominal", val, Math.Atan(2/1));

            // Zero first vector
            vec1 = new Vector3D(0, 0, 0);
            vec2 = new Vector3D(1, 0, 0);
            val = VectorMath.AngleBetween(vec1, vec2);
            Assert.IsEqual("AngleBetween Zero first vector", val, 0);

            // Zero second vector
            vec1 = new Vector3D(1, 0, 0);
            vec2 = new Vector3D(0, 0, 0);
            val = VectorMath.AngleBetween(vec1, vec2);
            Assert.IsEqual("AngleBetween Zero second vector", val, 0);
            #endregion

            #region IsDotProductWithinTolerance
            vec1 = new Vector3D(12, 2, 17);
            vec2 = new Vector3D(1, 24, -22);
            double cosTol = VectorMath.CosBetween(vec1, vec2);

            // Exactly on tolerance
            // Should return false as it is not inclusive
            Assert.IsFalse("IsDotProductWithinTolerance Right on tolerance", VectorMath.IsDotProductWithinTolerance(vec1, vec2, cosTol));

            // Within tolerance
            Assert.IsTrue("IsDotProductWithinTolerance Within tolerance", VectorMath.IsDotProductWithinTolerance(vec1, vec2, cosTol - 1e1));

            // Outside tolerance
            Assert.IsFalse("IsDotProductWithinTolerance Outside tolerance", VectorMath.IsDotProductWithinTolerance(vec1, vec2, cosTol + 1e1));

            #endregion
        }
    }
}
