﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace se_unit_tests.UnitTests
{
    /// <summary>
    /// Generic class for specifying unit tests
    /// </summary>
    public abstract class UT_Base
    {
        public string Name = "Base";
        public abstract void Run();
    }
}
