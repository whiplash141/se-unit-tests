﻿using System;

namespace se_unit_tests.UnitTests
{
    public struct ComplexNumber
    {
        public readonly double Real;
        public readonly double Imaginary;
        private const double Epsilon = 1e-9;

        public static readonly ComplexNumber Zero = new ComplexNumber(0, 0);

        public ComplexNumber(double real, double imaginary)
        {
            Real = real;
            Imaginary = imaginary;
        }

        #region Negation
        public static ComplexNumber operator -(ComplexNumber s1)
        {
            return new ComplexNumber(-s1.Real, -s1.Imaginary);
        }
        #endregion

        #region Addition
        public static ComplexNumber operator +(ComplexNumber s1, ComplexNumber s2)
        {
            return new ComplexNumber(s1.Real + s2.Real, s1.Imaginary + s2.Imaginary);
        }

        public static ComplexNumber operator +(ComplexNumber s1, double num)
        {
            return s1 + new ComplexNumber(num, 0);
        }

        public static ComplexNumber operator +(double num, ComplexNumber s1)
        {
            return new ComplexNumber(num, 0) + s1;
        }
        #endregion

        #region Subtraction
        public static ComplexNumber operator -(ComplexNumber s1, ComplexNumber s2)
        {
            return new ComplexNumber(s1.Real - s2.Real, s1.Imaginary - s2.Imaginary);
        }

        public static ComplexNumber operator -(ComplexNumber s1, double num)
        {
            return s1 - new ComplexNumber(num, 0);
        }

        public static ComplexNumber operator -(double num, ComplexNumber s1)
        {
            return new ComplexNumber(num, 0) - s1;
        }

        #endregion

        #region Multiplacation
        public static ComplexNumber operator *(ComplexNumber s1, ComplexNumber s2)
        {
            return new ComplexNumber(s1.Real * s2.Real - s1.Imaginary * s2.Imaginary, s1.Imaginary * s2.Real + s1.Real * s2.Imaginary);
        }

        public static ComplexNumber operator *(double num, ComplexNumber s1)
        {
            return new ComplexNumber(num, 0) * s1;
        }

        public static ComplexNumber operator *(ComplexNumber s1, double num)
        {
            return s1 * new ComplexNumber(num, 0);
        }
        #endregion

        #region Division
        public static ComplexNumber operator /(ComplexNumber s1, ComplexNumber s2)
        {
            double denom = s2.MagnitudeSquared();
            ComplexNumber reciprocal = new ComplexNumber(s2.Real / denom, -s2.Imaginary / denom);
            return s1 * reciprocal;
        }

        public static ComplexNumber operator /(double num, ComplexNumber s1)
        {
            return new ComplexNumber(num, 0) / s1;
        }

        public static ComplexNumber operator /(ComplexNumber s1, double num)
        {
            return s1 / new ComplexNumber(num, 0);
        }
        #endregion

        #region Equality
        public override bool Equals(Object s1)
        {
            return s1 is ComplexNumber && (ComplexNumber)s1 == new ComplexNumber(Real, Imaginary);
        }

        public override int GetHashCode()
        {
            return Real.GetHashCode() ^ Imaginary.GetHashCode();
        }

        public static bool operator ==(ComplexNumber s1, ComplexNumber s2)
        {
            return Math.Abs(s1.Real - s2.Real) < Epsilon && Math.Abs(s1.Imaginary - s2.Imaginary) < Epsilon;
        }

        public static bool operator !=(ComplexNumber s1, ComplexNumber s2)
        {
            return Math.Abs(s1.Real - s2.Real) > Epsilon || Math.Abs(s1.Imaginary - s2.Imaginary) > Epsilon;
        }
        #endregion

        #region Complex Number Properties
        public ComplexNumber Conjugate()
        {
            return new ComplexNumber(Real, -Imaginary);
        }

        public double Angle()
        {
            return Math.Atan2(Imaginary, Real);
        }

        public double MagnitudeSquared()
        {
            return Real * Real + Imaginary * Imaginary;
        }

        public double Magnitude()
        {
            return Math.Sqrt(MagnitudeSquared());
        }

        public bool IsReal
        {
            get
            {
                return Math.Abs(Imaginary) < Epsilon;
            }
        }

        public bool IsImaginary
        {
            get
            {
                return Math.Abs(Imaginary) > Epsilon && Math.Abs(Real) < Epsilon;
            }
        }

        public bool IsComplex
        {
            get
            {
                return Math.Abs(Imaginary) > Epsilon;
            }
        }
        #endregion

        #region Roots
        public static double? RealRoot(ComplexNumber s1, uint root)
        {
            for (uint ii = 0; ii < root; ++ii)
            {
                var num = Root(s1, root, ii);
                if (num.IsReal)
                {
                    return num.Real;
                }
            }
            return null;
        }

        public static double? RealRoot(double num, uint root)
        {
            ComplexNumber s1 = new ComplexNumber(num, 0);
            return RealRoot(s1, root);
        }

        public static ComplexNumber PrincipalRoot(ComplexNumber s1, uint root)
        {
            ComplexNumber maxReal = new ComplexNumber(double.MinValue, 0);
            for (uint ii = 0; ii < root; ++ii)
            {
                var r = Root(s1, root, ii);
                if (r.Real > maxReal.Real)
                {
                    maxReal = r;
                }
            }
            return maxReal;
        }

        public static ComplexNumber PrincipalRoot(double num, uint root)
        {
            ComplexNumber s1 = new ComplexNumber(num, 0);
            return PrincipalRoot(s1, root);
        }

        public static ComplexNumber Root(ComplexNumber s1, uint root, uint index = 0)
        {
            index = Math.Min(root - 1, index);

            double magnitude = s1.Magnitude();
            double angle = s1.Angle();
            double offset = index * 2 * Math.PI;
            return Math.Pow(magnitude, 1.0 / root) * new ComplexNumber(Math.Cos((angle + offset) / root), Math.Sin((angle + offset) / root));
        }

        public static ComplexNumber Root(double num, uint root, uint index = 0)
        {
            var s1 = new ComplexNumber(num, 0);
            return Root(s1, root, index);
        }

        public static ComplexNumber Sqrt(double num, uint index = 0)
        {
            return Root(num, 2, index);
        }
        #endregion

        #region Exponents
        public static ComplexNumber Pow(ComplexNumber s1, uint power)
        {
            double magnitude = s1.Magnitude();
            double angle = s1.Angle();
            return Math.Pow(magnitude, power) * new ComplexNumber(Math.Cos(power * angle), Math.Sin(power * angle));
        }


        public static ComplexNumber Square(ComplexNumber s1)
        {
            return s1 * s1;
        }

        #endregion

        public override string ToString()
        {
            string connector = Imaginary < 0 ? "-" : "+";
            return $"{Real} {connector} {Math.Abs(Imaginary)}i";
        }
    }
}
