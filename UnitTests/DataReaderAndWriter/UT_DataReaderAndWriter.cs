﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace se_unit_tests.UnitTests
{
    class UT_DataReaderAndWriter : UT_Base
    {
        public override void Run()
        {

            var writer = new DataWriter();

            {
                byte by = 250;
                bool b = true;
                short s = -31582;
                ushort us = 3345;
                int i = -69420;
                uint ui = 35912;
                long l = -14325653;
                ulong ul = 83764;
                float f = -3435.112f;
                double d = 45.56;

                writer.Initialize();
                writer.Write(by);
                writer.Write(b);
                writer.Write(s);
                writer.Write(us);
                writer.Write(i);
                writer.Write(ui);
                writer.Write(l);
                writer.Write(ul);
                writer.Write(f);
                writer.Write(d);
            }
            string result = writer.ToString();

            var reader = new DataReader();
            {
                byte by;
                bool b;
                short s;
                ushort us;
                int i;
                uint ui;
                long l;
                ulong ul;
                float f;
                double d;
                reader.Initialize(result);
                reader.Read(out by);
                reader.Read(out b);
                reader.Read(out s);
                reader.Read(out us);
                reader.Read(out i);
                reader.Read(out ui);
                reader.Read(out l);
                reader.Read(out ul);
                reader.Read(out f);
                reader.Read(out d);
                Assert.IsEqual("byte", by, 250);
                Assert.IsTrue("bool", b);
                Assert.IsEqual("short", s, -31582);
                Assert.IsEqual("ushort", us, 3345);
                Assert.IsEqual("int", i, -69420);
                Assert.IsEqual("uint", ui, 35912);
                Assert.IsEqual("long", l, -14325653);
                Assert.IsEqual("ulong", ul, 83764);
                Assert.IsEqual("float", f, -3435.112f);
                Assert.IsEqual("double", d, 45.56);
            }

        }
    }
}
