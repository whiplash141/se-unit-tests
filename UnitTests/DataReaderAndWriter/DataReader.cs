﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace se_unit_tests.UnitTests
{
    class DataReader
    {
        byte[] _data = null;
        int _offset = 0;

        public void Initialize(string str)
        {
            _data = Encoding.Default.GetBytes(str);
        }

        public void Read(out byte val)
        {
            val = _data[_offset];
            _offset += sizeof(byte);
        }

        public void Read(out char val)
        {
            val = BitConverter.ToChar(_data, _offset);
            _offset += sizeof(char);
        }

        public void Read(out bool val)
        {
            val = BitConverter.ToBoolean(_data, _offset);
            _offset += sizeof(bool);
        }

        public void Read(out short val)
        {
            val = BitConverter.ToInt16(_data, _offset);
            _offset += sizeof(short);
        }

        public void Read(out ushort val)
        {
            val = BitConverter.ToUInt16(_data, _offset);
            _offset += sizeof(ushort);
        }

        public void Read(out int val)
        {
            val = BitConverter.ToInt32(_data, _offset);
            _offset += sizeof(int);
        }

        public void Read(out uint val)
        {
            val = BitConverter.ToUInt32(_data, _offset);
            _offset += sizeof(uint);
        }

        public void Read(out long val)
        {
            val = BitConverter.ToInt64(_data, _offset);
            _offset += sizeof(long);
        }

        public void Read(out ulong val)
        {
            val = BitConverter.ToUInt64(_data, _offset);
            _offset += sizeof(ulong);
        }

        public void Read(out float val)
        {
            double dbl;
            Read(out dbl);
            val = (float)dbl; // Floats are stored as doubles
        }

        public void Read(out double val)
        {
            val = BitConverter.ToDouble(_data, _offset);
            _offset += sizeof(double);
        }
    }


}
