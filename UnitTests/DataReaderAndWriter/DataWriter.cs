﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace se_unit_tests.UnitTests
{  
    class DataWriter
    {
        List<byte> _dataBuilder = new List<byte>();
        byte[] _data = null;
        int _lastNumBytes = -1;

        public DataWriter(int size = 64)
        {
            _dataBuilder = new List<byte>(size);
        }

        public override string ToString()
        {
            int numBytes = _dataBuilder.Count;
            if (numBytes != _lastNumBytes || _data == null)
            {
                _data = new byte[numBytes];
                _lastNumBytes = numBytes;
            }
            for (int ii = 0; ii < numBytes; ++ii)
            {
                _data[ii] = _dataBuilder[ii];
            }
            return Encoding.Default.GetString(_data);
        }

        public void Initialize()
        {
            _dataBuilder.Clear();
        }

        public void Write(byte val)
        {
            AddBytes(val, sizeof(byte));
        }

        public void Write(char val)
        {
            AddBytes(val, sizeof(char));
        }

        public void Write(bool val)
        {
            AddBytes(val ? 1 : 0, sizeof(bool));
        }

        public void Write(short val)
        {
            AddBytes(val, sizeof(short));
        }

        public void Write(ushort val)
        {
            AddBytes(val, sizeof(ushort));
        }

        public void Write(int val)
        {
            AddBytes(val, sizeof(int));
        }

        public void Write(uint val)
        {
            AddBytes(val, sizeof(uint));
        }

        public void Write(long val)
        {
            AddBytes(val, sizeof(long));
        }

        public void Write(ulong val)
        {
            AddBytes(val, sizeof(ulong));
        }

        public void Write(float val)
        {
            Write((double)val); // Floats are cast to double
        }

        public void Write(double val)
        {
            long bits = BitConverter.DoubleToInt64Bits(val);
            AddBytes(bits, sizeof(double));
        }

        void AddBytes(long bits, int numBytes)
        {
            AddBytes((ulong)bits, numBytes);
        }

        void AddBytes(ulong bits, int numBytes)
        {
            for (int byteIdx = 0; byteIdx < numBytes; ++byteIdx)
            {
                int offset = 8 * byteIdx;
                _dataBuilder.Add((byte)((bits & (0xFFul << offset)) >> offset));
            }
        }
    }


}
