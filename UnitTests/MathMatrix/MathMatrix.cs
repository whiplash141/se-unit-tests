using VRageMath;
using System;
using System.Text;

namespace se_unit_tests.UnitTests
{
    public struct MathMatrix : IEquatable<MathMatrix>
    {
        #region Fields
        private static StringBuilder _sb = new StringBuilder();
        private readonly double[,] _raw;
        readonly uint _rows;
        readonly uint _cols;
        bool _isTransposed;
        #endregion

        #region Properties
        public uint Rows
        {
            get
            {
                return !_isTransposed ? _rows : _cols;
            }
        }

        public uint Cols
        {
            get
            {
                return !_isTransposed ? _cols : _rows;
            }
        }
        #endregion

        #region Constructors
        public MathMatrix(uint rows, uint cols)
        {
            _isTransposed = false;
            _rows = rows;
            _cols = cols;
            _raw = new double[_rows, _cols];
        }

        public MathMatrix(double[,] arr)
        {
            _isTransposed = false;
            _rows = (uint)arr.GetLength(0);
            _cols = (uint)arr.GetLength(1);
            _raw = new double[_rows, _cols];
            Array.Copy(arr, _raw, arr.Length);
        }

        public MathMatrix(Matrix3x3 other)
        {
            _isTransposed = false;
            _rows = _cols = 3;
            _raw = new double[3, 3];
            for (int r = 0; r < Rows; ++r)
            {
                for (int c = 0; c < Cols; ++c)
                {
                    this[r, c] = other[r, c];
                }
            }
        }

        public MathMatrix(MatrixD other)
        {
            _isTransposed = false;
            _rows = _cols = 4;
            _raw = new double[_rows, _cols];
            for (int r = 0; r < Rows; ++r)
            {
                for (int c = 0; c < Cols; ++c)
                {
                    this[r, c] = other[r, c];
                }
            }
        }

        public MathMatrix(MathMatrix other)
        {
            _isTransposed = false;
            _rows = other.Rows;
            _cols = other.Cols;
            _raw = new double[_rows, _cols];
            Array.Copy(other._raw, _raw, other._raw.Length);
        }
        #endregion

        #region Methods
        public MathMatrix Clone()
        {
            return new MathMatrix(this);
        }

        public static MathMatrix Identity(uint size)
        {
            var mat = new MathMatrix(size, size);
            for (int dim = 0; dim < size; ++ dim)
            {
                mat[dim, dim] = 1.0;
            }
            return mat;
        }

        public static MathMatrix Ones(uint rows, uint cols)
        {
            var mat = new MathMatrix(rows, cols);
            for (int r = 0; r < mat.Rows; ++r)
            {
                for (int c = 0; c < mat.Cols; ++c)
                {
                    mat[r, c] = 1.0;
                }
            }
            return mat;
        }

        public static MathMatrix Transpose(MathMatrix mat)
        {
            var res = new MathMatrix(mat);
            res.TransposeInPlace();
            return res;
        }

        public void TransposeInPlace()
        {
            _isTransposed = !_isTransposed;
        }
        #endregion

        #region Equality
        public bool Equals(MathMatrix other, double epsilon)
        {
            if (Rows != other.Rows || Cols != other.Cols)
            {
                return false;
            }
            for (int r = 0; r < Rows; ++r)
            {
                for (int c = 0; c < Cols; ++c)
                {
                    if (Math.Abs(this[r, c] - other[r, c]) > epsilon)
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public bool Equals(MathMatrix other)
        {
            return Equals(other, 0);
        }

        public override bool Equals(object obj)
        {
            bool result = false;
            if (obj is MathMatrix)
            {
                result = Equals((MathMatrix)obj);
            }
            return result;
        }

        public override int GetHashCode()
        {
            int hash = 0;
            for (int r = 0; r < Rows; ++r)
            {
                for (int c = 0; c < Cols; ++c)
                {
                    hash += this[r, c].GetHashCode();
                }
            }
            return hash;
        }
        #endregion

        #region Accesors
        public double this[uint r, uint c]
        {
            get
            {
                return Get(r, c);
            }
            set
            {
                Set(r, c, value);
            }
        }

        public double this[int r, int c]
        {
            get
            {
                return Get((uint)r, (uint)c);
            }
            set
            {
                Set((uint)r, (uint)c, value);
            }
        }

        private double Get(uint r, uint c)
        {
            if (!_isTransposed)
            {
                return _raw[r, c];
            }
            else
            {
                return _raw[c, r];
            }
        }

        private void Set(uint r, uint c, double val)
        {
            if (!_isTransposed)
            {
                _raw[r, c] = val;
            }
            else
            {
                _raw[c, r] = val;
            }
        }
        #endregion

        #region Math Operations
        #region Multiplication
        public static MathMatrix operator *(MathMatrix m1, MathMatrix m2)
        {
            if (m1.Cols != m2.Rows)
            {
                throw new ArgumentException($"Number of columns of m1 must equal number of rows of m2 ({m1.Cols} != {m2.Rows})");
            }
            var res = new MathMatrix(m1.Rows, m2.Cols);
            for (int r = 0; r < m1.Rows; ++r)
            {
                for (int c = 0; c < m2.Cols; ++c)
                {
                    double num = 0;
                    for (int e = 0; e < m1.Cols; ++e)
                    {
                        num += m1[r, e] * m2[e, c];
                    }
                    res[r, c] = num;
                }
            }
            return res;
        }

        public static MathMatrix operator *(MathMatrix m1, double d1)
        {
            var res = new MathMatrix(m1.Rows, m1.Cols);
            for (int r = 0; r < res.Rows; ++r)
            {
                for (int c = 0; c < res.Cols; ++c)
                {
                    res[r, c] = m1[r, c] * d1;
                }
            }
            return res;
        }

        public static MathMatrix operator *(double d1, MathMatrix m1)
        {
            return m1 * d1;
        }
        #endregion

        #region Division
        public static MathMatrix operator /(MathMatrix m1, double d1)
        {
            double div = 1.0 / d1;
            return m1 * div;
        }

        public static MathMatrix operator /(double d1, MathMatrix m1)
        {
            var res = new MathMatrix(m1.Rows, m1.Cols);
            for (uint r = 0; r < res.Rows; ++r)
            {
                for (uint c = 0; c < res.Cols; ++c)
                {
                    res[r, c] = d1 / m1[r, c];
                }
            }
            return res;
        }
        #endregion

        #region Negation   
        public static MathMatrix operator -(MathMatrix m1)
        {
            var res = new MathMatrix(m1.Rows, m1.Cols);
            for (int r = 0; r < res.Rows; ++r)
            {
                for (int c = 0; c < res.Cols; ++c)
                {
                    res[r, c] = -m1[r, c];
                }
            }
            return res;
        }
        #endregion

        #region Addition/Subtraction
        public static MathMatrix operator +(MathMatrix m1, MathMatrix m2)
        {
            if (m1.Rows != m2.Rows)
            {
                throw new ArgumentException($"Number of rows of m1 must equal number of rows of m2 ({m1.Rows} != {m2.Rows})");
            }

            if (m1.Cols != m2.Cols)
            {
                throw new ArgumentException($"Number of columns of m1 must equal number of columns of m2 ({m1.Cols} != {m2.Cols})");
            }

            var res = new MathMatrix(m1.Rows, m1.Cols);
            for (int r = 0; r < res.Rows; ++r)
            {
                for (int c = 0; c < res.Cols; ++c)
                {
                    res[r, c] = m1[r, c] + m2[r, c];
                }
            }
            return res;
        }

        public static MathMatrix operator -(MathMatrix m1, MathMatrix m2)
        {
            if (m1.Rows != m2.Rows)
            {
                throw new ArgumentException($"Number of rows of m1 must equal number of rows of m2 ({m1.Rows} != {m2.Rows})");
            }

            if (m1.Cols != m2.Cols)
            {
                throw new ArgumentException($"Number of columns of m1 must equal number of columns of m2 ({m1.Cols} != {m2.Cols})");
            }

            var res = new MathMatrix(m1.Rows, m1.Cols);
            for (int r = 0; r < res.Rows; ++r)
            {
                for (int c = 0; c < res.Cols; ++c)
                {
                    res[r, c] = m1[r, c] - m2[r, c];
                }
            }
            return res;
        }
        #endregion

        #region Slicing
        MathMatrix GetSubMatrix(uint startRow, uint startCol, uint endRow, uint endCol)
        {
            // TODO: Assert size
            uint rows = endRow - startRow + 1;
            uint cols = endCol - startCol + 1;
            var sub = new MathMatrix(rows, cols);
            for (uint r = 0; r < rows; ++r)
            {
                for (uint c = 0; c < cols; ++c)
                {
                    sub[r, c] = this[r + startRow, c + startCol];
                }
            }
            return sub;
        }

        void SetSubMatrix(MathMatrix sub, uint startRow, uint startCol, uint endRow, uint endCol)
        {
            // TODO: Assert size
            uint rows = endRow - startRow + 1;
            uint cols = endCol - startCol + 1;
            for (uint r = 0; r < rows; ++r)
            {
                for (uint c = 0; c < cols; ++c)
                {
                    this[r + startRow, c + startCol] = sub[r, c];
                }
            }
        }
        #endregion

        #region Factorization
        /// <summary>
        /// Recursive leading-row-column LU algorithm
        /// </summary>
        /// <see cref="https://courses.engr.illinois.edu/cs357/fa2019/references/ref-7-linsys/"/>
        /// <param name="A"></param>
        /// <param name="L"></param>
        /// <param name="U"></param>
        /// <returns></returns>
        public static bool LUDecomposition(ref MathMatrix A, out MathMatrix L, out MathMatrix U)
        {
            if (A.Rows != A.Cols)
            {
                throw new ArgumentException("LUDecomposition can only be called on square matrices");
            }

            uint N = A.Rows;
            L = new MathMatrix(N, N);
            U = new MathMatrix(N, N);

            return LUDecompositionImpl(A, ref L, ref U);
        }

        static bool LUDecompositionImpl(MathMatrix A, ref MathMatrix L, ref MathMatrix U)
        {
            uint N = A.Rows;

            if (N == 1)
            {
                L[0, 0] = 1;
                U = A.Clone();
                return true;
            }

            double A11 = A[0,0];
            MathMatrix A12 = A.GetSubMatrix(0, 1, 0, N - 1);
            MathMatrix A21 = A.GetSubMatrix(1, 0, N - 1, 0);
            MathMatrix A22 = A.GetSubMatrix(1, 1, N - 1, N - 1);

            double L11 = 0;
            MathMatrix L12 = L.GetSubMatrix(0, 1, 0, N - 1);
            MathMatrix L21 = L.GetSubMatrix(1, 0, N - 1, 0);
            MathMatrix L22 = L.GetSubMatrix(1, 1, N - 1, N - 1);

            double U11 = 0;
            MathMatrix U12 = U.GetSubMatrix(0, 1, 0, N - 1);
            MathMatrix U21 = U.GetSubMatrix(1, 0, N - 1, 0);
            MathMatrix U22 = U.GetSubMatrix(1, 1, N - 1, N - 1);

            L11 = 1;
            U11 = A11;

            L12 *= 0;
            U12 = A12.Clone();

            if (Math.Abs(U11) < 1e-6)
            {
                return false;
            }
            L21 = A21.Clone() / U11;
            U21 *= 0;

            MathMatrix S22 = A22 - (L21 * U12);
            if (!LUDecompositionImpl(S22, ref L22, ref U22))
            {
                return false;
            }

            L[0, 0] = L11;
            L.SetSubMatrix(L12, 0, 1, 0, N - 1);
            L.SetSubMatrix(L21, 1, 0, N - 1, 0);
            L.SetSubMatrix(L22, 1, 1, N - 1, N - 1);

            U[0, 0] = U11;
            U.SetSubMatrix(U12, 0, 1, 0, N - 1);
            U.SetSubMatrix(U21, 1, 0, N - 1, 0);
            U.SetSubMatrix(U22, 1, 1, N - 1, N - 1);

            return true;
        }

        public static bool ForwardSub(MathMatrix L, MathMatrix b, out MathMatrix y)
        {
            uint N = L.Rows;
            y = new MathMatrix(N, 1);
            for (int i = 0; i < N; ++i)
            {
                double sum = 0;
                for (int j = 0; j < i; ++j)
                {
                    sum += y[j, 0] * L[i, j];
                }

                if (Math.Abs(L[i, i]) < 1e-6)
                {
                    return false;
                }
                y[i, 0] = (b[i, 0] - sum) / L[i, i];
            }
            return true;
        }

        public static bool BackSub(MathMatrix U, MathMatrix y, out MathMatrix x)
        {
            uint N = U.Rows;
            x = new MathMatrix(N, 1);
            for (int i = (int)N - 1; i >= 0; --i)
            {
                double sum = 0;
                for (int j = i + 1; j < N; ++j)
                {
                    sum += x[j, 0] * U[i, j];
                }

                if (Math.Abs(U[i, i]) < 1e-6)
                {
                    return false;
                }
                x[i, 0] = (y[i, 0] - sum) / U[i, i];
            }
            return true;
        }

        public static bool Inverse(ref MathMatrix A, out MathMatrix AInv)
        {
            if (A.Rows != A.Cols)
            {
                throw new ArgumentException("Inverse can only be called on square matrices");
            }

            uint N = A.Rows;
            AInv = new MathMatrix(N, N);
            
            // Find L and U matrices
            MathMatrix L, U;
            if (!LUDecomposition(ref A, out L, out U))
            {
                return false;
            }

            // Solve for inverse of U
            var UAugmented = new MathMatrix(N, 2 * N);
            UAugmented.SetSubMatrix(U, 0, 0, N - 1, N - 1);
            UAugmented.SetSubMatrix(Identity(N), 0, N, N - 1, 2 * N - 1);
            for (uint i = 0; i < N; ++ i)
            {
                MathMatrix currentRow = UAugmented.GetSubMatrix(i, 0, i, 2 * N - 1);
                for (uint j = i+1; j < N; ++j)
                {
                    MathMatrix row = UAugmented.GetSubMatrix(j, 0, j, 2 * N - 1);

                    if (Math.Abs(row[0, j]) < 1e-6)
                    {
                        return false;
                    }
                    double scale = currentRow[0, j] / row[0, j];

                    currentRow -= (scale * row);
                }

                if (Math.Abs(currentRow[0, i]) < 1e-6)
                {
                    return false;
                }
                double scale2 = 1 / currentRow[0, i];

                currentRow *= scale2;
                UAugmented.SetSubMatrix(currentRow, i, 0, i, 2 * N - 1);
            }
            MathMatrix UInv = UAugmented.GetSubMatrix(0, N, N - 1, 2 * N - 1);

            // Solve for inverse of L
            var LAugmented = new MathMatrix(N, 2 * N);
            LAugmented.SetSubMatrix(L, 0, 0, N - 1, N - 1);
            LAugmented.SetSubMatrix(Identity(N), 0, N, N - 1, 2 * N - 1);
            for (int i = (int)N - 1; i >= 0; --i)
            {
                MathMatrix currentRow = LAugmented.GetSubMatrix((uint)i, 0, (uint)i, 2 * N - 1);
                for (int j = i - 1; j >= 0; --j)
                {
                    MathMatrix row = LAugmented.GetSubMatrix((uint)j, 0, (uint)j, 2 * N - 1);

                    if (Math.Abs(row[0, j]) < 1e-6)
                    {
                        return false;
                    }
                    double scale = currentRow[0, j] / row[0, j];
                    currentRow -= (scale * row);
                }

                if (Math.Abs(currentRow[0, i]) < 1e-6)
                {
                    return false;
                }
                double scale2 = 1 / currentRow[0, i];
                currentRow *= scale2;

                LAugmented.SetSubMatrix(currentRow, (uint)i, 0, (uint)i, 2 * N - 1);
            }
            MathMatrix LInv = LAugmented.GetSubMatrix(0, N, N - 1, 2 * N - 1);

            // A = L*U
            // inv(A) = inv(L*U)
            // inv(L*U) = inv(U)*inv(L)
            AInv = UInv * LInv;
            return true;
        }
        
        public static bool PsuedoInverse(ref MathMatrix A, out MathMatrix APinv)
        {
            APinv = new MathMatrix(A.Cols, A.Rows);

            MathMatrix ATpose = Transpose(A);
            MathMatrix ATA = ATpose * A;
            MathMatrix ATAInv;
            if (!Inverse(ref ATA, out ATAInv))
            {
                MathMatrix AAT = A * ATpose;
                MathMatrix AATInv;
                if (!Inverse(ref ATA, out AATInv))
                {
                    return false;
                }
                APinv = ATpose * AATInv;
                return true;
            }

            APinv = ATAInv * ATpose;

            return true;
        }
        #endregion
        #endregion

        public override string ToString()
        {
            _sb.Clear();
            _sb.Append("[");
            for (int r = 0; r < Rows; ++r)
            {
                _sb.Append("[");
                for (int c = 0; c < Cols; ++c)
                {
                    _sb.Append(this[r,c]);
                    if (c < Cols - 1)
                    {
                        _sb.Append(", ");
                    }
                }
                _sb.Append("]");
                if (r < Rows -1)
                {
                    _sb.Append(", ");
                }
            }
            _sb.Append("]");
            return _sb.ToString();
        }
    }
}
