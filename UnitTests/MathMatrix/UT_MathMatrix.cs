﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using se_unit_tests.UnitTests;
using VRageMath;

namespace se_unit_tests.UnitTests
{
    /// <summary>
    /// Unit tests for vector math class.
    /// </summary>
    class UT_MathMatrix : UT_Base
    {
        public UT_MathMatrix() 
        {
            this.Name = "MathMatrix";
        }

        public override void Run()
        {

            #region Constructors
            {
                var mat = new MathMatrix(2, 4);
                MatrixEqual("Construction from rows and cols", mat, 
                    new double[,] 
                    { 
                        { 0,0,0,0 },
                        { 0,0,0,0 }
                    }
                );
            }

            {
                var source = new double[,] { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 }, { 10, 11, 12 } };
                var mat = new MathMatrix(source);
                // To assert deep copy
                source[0, 0] = 100;
                MatrixEqual("Construction from multi-dimensional array", mat,
                    new double[,]
                    {
                        { 1, 2, 3 },
                        { 4, 5, 6 }, 
                        { 7, 8, 9 }, 
                        { 10, 11, 12 }
                    }
                );
            }

            {
                var source = new Matrix3x3(1, 2, 3, 4, 5, 6, 7, 8, 9);
                var mat = new MathMatrix(source);
                // To assert deep copy
                source[0, 0] = 100;
                MatrixEqual("Construction from Matrix3x3", mat,
                    new double[,]
                    {
                        { 1, 2, 3 },
                        { 4, 5, 6 },
                        { 7, 8, 9 },
                    }
                );
            }

            {
                var source = new MatrixD(11,12,13,14,21,22,23,24,31,32,33,34,41,42,43,44);
                var mat = new MathMatrix(source);
                // To assert deep copy
                source[0, 0] = 100;
                MatrixEqual("Construction from MatrixD", mat,
                    new double[,]
                    {
                        { 11,12,13,14 },
                        { 21,22,23,24 },
                        { 31,32,33,34 },
                        { 41,42,43,44 }
                    }
                );
            }

            {
                var m1 = new MathMatrix(new double[,]
                    {
                        { 1,2 },
                        { 3,4 }
                    }
                );
                var m2 = new MathMatrix(m1);
                // Assert deep copy
                m1[0, 0] = 69;
                MatrixEqual("Construction from MathMatrix", m2, new double[,]
                    {
                        { 1,2 },
                        { 3,4 }
                    }
                );
            }
            #endregion

            #region Methods
            {
                var m1 = new MathMatrix(new double[,]
                    {
                        { 1,2 },
                        { 3,4 }
                    }
                );
                var m2 = m1.Clone();
                m1[0, 0] = 69;
                MatrixEqual("Clone does a deep copy", m2, new double[,]
                    {
                        { 1,2 },
                        { 3,4 }
                    }
                );
            }

            {
                var mat = MathMatrix.Identity(3);
                MatrixEqual("Identity matrix", mat,
                    new double[,]
                    {
                        { 1,0,0 },
                        { 0,1,0 },
                        { 0,0,1 }
                    }
                );
            }

            {
                var mat = MathMatrix.Ones(1,3);
                MatrixEqual("Ones matrix", mat,
                    new double[,]
                    {
                        { 1,1,1 }
                    }
                );
            }

            {
                var source = new double[,] 
                {
                    { 1,3,5 },
                    { 7,9,11 }
                };
                var mat = new MathMatrix(source);
                mat.TransposeInPlace();
                MatrixEqual("Transpose in place", mat,
                    new double[,]
                    {
                        { 1,7 },
                        { 3,9 },
                        { 5,11 }
                    }
                );
            }

            {
                var source = new double[,]
                {
                    { 1,3,5 },
                    { 7,9,11 }
                };
                var toTpose = new MathMatrix(source);
                var mat = MathMatrix.Transpose(toTpose);
                MatrixEqual("Transpose", mat,
                    new double[,]
                    {
                        { 1,7 },
                        { 3,9 },
                        { 5,11 }
                    }
                );
                MatrixEqual("Transpose does not mutate source", toTpose,
                    new double[,]
                    {
                        { 1,3,5 },
                        { 7,9,11 }
                    }
                );
            }
            #endregion

            #region Equality
            {
                var m1 = new MathMatrix(new double[,]
                    {
                        { 1,2 },
                        { 3,4 }
                    }
                );
                var m2 = new MathMatrix(new double[,]
                    {
                        { 1,2 },
                        { 3,4 }
                    }
                );
                var m3 = new MathMatrix(new double[,]
                    {
                        { 1,2 },
                        { 3,4.1 }
                    }
                );
                Assert.IsTrue("Object equality", m1.Equals((object)m2));
                Assert.IsTrue("Typed equality", m1.Equals(m2));
                Assert.IsFalse("Typed equality - no epsilon", m1.Equals(m3));
                Assert.IsTrue("Typed equality - with epsilon", m1.Equals(m3, 0.11));
                Assert.IsEqual("Hash code equality", m1.GetHashCode(), m2.GetHashCode());
                Assert.IsNotEqual("Hash code inequality", m1.GetHashCode(), m3.GetHashCode());
            }
            #endregion

            #region Math
            #region Multiplication
            {
                var m1 = new MathMatrix(1, 3)
                {
                    [0, 0] = 1,
                    [0, 1] = 2,
                    [0, 2] = 4,
                };
                var m2 = new MathMatrix(3, 2)
                {
                    [0, 0] = 1,
                    [0, 1] = 4,

                    [1, 0] = 3,
                    [1, 1] = 2,

                    [2, 0] = 5,
                    [2, 1] = 10,
                };
                MathMatrix m3 = m1 * m2;
                MatrixEqual("Matrix-matrix multiplication", m3,
                    new double[,]
                    {
                        { 27,48 },
                    }
                );
            }

            {
                var m1 = new MathMatrix(1, 3)
                {
                    [0, 0] = 1,
                    [0, 1] = 2,
                    [0, 2] = 4,
                };
                var m2 = new MathMatrix(3, 2)
                {
                    [0, 0] = 1,
                    [0, 1] = 4,

                    [1, 0] = 3,
                    [1, 1] = 2,

                    [2, 0] = 5,
                    [2, 1] = 10,
                };
                bool exceptioned = false;
                try
                {
                    MathMatrix m3 = m2 * m1;
                }
                catch (ArgumentException)
                {
                    exceptioned = true;
                }
                Assert.IsTrue("Matrix-matrix multiplication fails with row/column mismatch", exceptioned);
            }

            {
                var m1 = new MathMatrix(3, 3)
                {
                    [0, 0] = 1,
                    [0, 1] = 2,
                    [0, 2] = 3,

                    [1, 0] = 4,
                    [1, 1] = 5,
                    [1, 2] = 6,

                    [2, 0] = 7,
                    [2, 1] = 8,
                    [2, 2] = 9,
                };
                MathMatrix m2 = m1 * 3;
                MatrixEqual("Matrix-scalar multiplication", m2,
                    new double[,]
                    {
                        { 3, 6, 9},
                        { 12, 15, 18 },
                        { 21, 24, 27 }
                    }
                );
            }

            {
                var m1 = new MathMatrix(3, 3)
                {
                    [0, 0] = 1,
                    [0, 1] = 2,
                    [0, 2] = 3,

                    [1, 0] = 4,
                    [1, 1] = 5,
                    [1, 2] = 6,

                    [2, 0] = 7,
                    [2, 1] = 8,
                    [2, 2] = 9,
                };
                MathMatrix m2 = 2 * m1;
                MatrixEqual("Scalar-matrix multiplication", m2,
                    new double[,]
                    {
                        { 2,4,6},
                        { 8,10,12 },
                        { 14,16,18 }
                    }
                );
            }
            #endregion

            #region Division
            {
                var m1 = MathMatrix.Ones(3,3);
                MathMatrix m2 = m1 / 2;
                MatrixEqual("Matrix-scalar division", m2,
                    new double[,]
                    {
                        { 0.5, 0.5, 0.5 },
                        { 0.5, 0.5, 0.5 },
                        { 0.5, 0.5, 0.5 },
                    }
                );
            }

            {
                var m1 = new MathMatrix(2, 2)
                {
                    [0, 0] = 1,
                    [0, 1] = 2,
                    [1, 0] = 4,
                    [1, 1] = 8,
                };
                MathMatrix m2 = 2 / m1;
                MatrixEqual("Scalar-matrix division", m2,
                    new double[,]
                    {
                        { 2, 1 },
                        { 0.5, 0.25 },
                    }
                );
            }
            #endregion

            #region Negation
            {
                var m1 = MathMatrix.Identity(3);
                MathMatrix m2 = -m1;
                MatrixEqual("Matrix negation", m2,
                    new double[,]
                    {
                        { -1,0,0 },
                        { 0,-1,0 },
                        { 0,0,-1 },
                    }
                );
            }
            #endregion

            #region Addition/Subtraction
            {
                var m1 = MathMatrix.Identity(3);
                var m2 = MathMatrix.Ones(3, 3);
                MathMatrix m3 = m2 + m1;
                MatrixEqual("Matrix-matrix addition", m3,
                    new double[,]
                    {
                        { 2,1,1 },
                        { 1,2,1 },
                        { 1,1,2 },
                    }
                );
            }

            {
                var m1 = MathMatrix.Identity(3);
                var m2 = MathMatrix.Ones(3, 2);
                bool exceptioned = false;
                try
                {
                    MathMatrix m3 = m2 + m1;
                }
                catch (ArgumentException)
                {
                    exceptioned = true;
                }
                Assert.IsTrue("Matrix-matrix addition exceptions on column mismatch", exceptioned);
            }

            {
                var m1 = MathMatrix.Identity(3);
                var m2 = MathMatrix.Ones(2, 3);
                bool exceptioned = false;
                try
                {
                    MathMatrix m3 = m2 + m1;
                }
                catch (ArgumentException)
                {
                    exceptioned = true;
                }
                Assert.IsTrue("Matrix-matrix addition exceptions on row mismatch", exceptioned);
            }

            {
                var m1 = MathMatrix.Identity(3);
                var m2 = MathMatrix.Ones(3, 3);
                MathMatrix m3 = m2 - m1;
                MatrixEqual("Matrix-matrix subtraction", m3,
                    new double[,]
                    {
                        { 0,1,1 },
                        { 1,0,1 },
                        { 1,1,0 },
                    }
                );
            }

            {
                var m1 = MathMatrix.Identity(3);
                var m2 = MathMatrix.Ones(3, 2);
                bool exceptioned = false;
                try
                {
                    MathMatrix m3 = m2 - m1;
                }
                catch (ArgumentException)
                {
                    exceptioned = true;
                }
                Assert.IsTrue("Matrix-matrix subtraction exceptions on column mismatch", exceptioned);
            }

            {
                var m1 = MathMatrix.Identity(3);
                var m2 = MathMatrix.Ones(2, 3);
                bool exceptioned = false;
                try
                {
                    MathMatrix m3 = m2 - m1;
                }
                catch (ArgumentException)
                {
                    exceptioned = true;
                }
                Assert.IsTrue("Matrix-matrix subtraction exceptions on row mismatch", exceptioned);
            }
            #endregion

            #region Factorization
            {
                var m1 = new MathMatrix(new double[,]
                    {
                        {1,1,1},
                        {16,4,1},
                        {4,2,1},
                    }
                );

                var b = new MathMatrix(new double[,]
                    {
                        { 0 },
                        { 2 },
                        { 1 },
                    }
                );

                MathMatrix L, U;
                Assert.IsTrue("LUDecomposition succeeds", MathMatrix.LUDecomposition(ref m1, out L, out U));
                MatrixEqual("LUDecomposition is equivalent to input", L * U, new double[,]
                    {
                        {1,1,1},
                        {16,4,1},
                        {4,2,1},
                    } 
                );

                MathMatrix y, x;
                Assert.IsTrue("ForwardSub succeeds", MathMatrix.ForwardSub(L, b, out y));
                Assert.IsTrue("BackSub succeeds", MathMatrix.BackSub(U, y, out x));
                MatrixEqual("LUDecomposition solves equation", x, new double[,]
                    {
                        { -0.166666666666667 },
                        { 1.500000000000000 },
                        { -1.333333333333333 }
                    }
                );

                MathMatrix m1Inv;
                Assert.IsTrue("Matrix inverse succeeds", MathMatrix.Inverse(ref m1, out m1Inv));
                MatrixEqual("Matrix inverse value is expected", m1Inv, new double[,]
                    {
                        { 0.333333333333333, 0.166666666666667, -0.500000000000000 },
                        { -2.000000000000000, -0.500000000000000, 2.500000000000000 },
                        { 2.666666666666667, 0.333333333333333, -2.000000000000000 },
                    }
                );
            }
            #endregion

            #region Inverse
            {
                var m1 = new MathMatrix(new double[,]
                    {
                        {1,1,1},
                        {16,4,1},
                        {4,2,1},
                    }
                );

                MathMatrix m1Inv;
                Assert.IsTrue("Matrix inverse succeeds", MathMatrix.Inverse(ref m1, out m1Inv));
                MatrixEqual("Matrix inverse value is expected", m1Inv, new double[,]
                    {
                        { 0.333333333333333, 0.166666666666667, -0.500000000000000 },
                        { -2.000000000000000, -0.500000000000000, 2.500000000000000 },
                        { 2.666666666666667, 0.333333333333333, -2.000000000000000 },
                    }
                );
            }
            #endregion

            #region Pseudo-inverse
            {
                var m1 = new MathMatrix(new double[,]
                    {
                        { 1,2,0},
                        { 0,0,1 }
                        //{ 0, 0, 1},
                        //{ 1, 1, 1},
                        //{ 9, 3, 1},
                        //{ 16, 4, 1},
                    }
                );

                MathMatrix m1Pinv;
                Assert.IsTrue("Matrix pseudo-inverse succeeds", MathMatrix.PsuedoInverse(ref m1, out m1Pinv));
                MatrixEqual("Matrix pseudo-inverse is expected value", m1Pinv, new double[,]
                    {
                        { 0.166666666666667, -0.166666666666667, -0.166666666666667, 0.166666666666667 },
                        { -0.866666666666667, 0.566666666666667, 0.766666666666667, -0.466666666666667 },
                        { 0.900000000000000, 0.200000000000000, -0.200000000000000, 0.100000000000000 },
                    }
                );
            }
            #endregion
            #endregion

        }

        void MatrixEqual(string title, MathMatrix actual, double[,] expected, double epsilon = 1e-6)
        {
            if (actual.Rows != expected.GetLength(0))
            {
                Assert.Fail(title, $"Different number of rows ({actual.Rows} vs {expected.GetLength(0)})");
                return;
            }

            if (actual.Cols != expected.GetLength(1))
            {
                Assert.Fail(title, $"Different number of columns ({actual.Cols} vs {expected.GetLength(1)})");
                return;
            }

            for (uint ii = 0; ii < actual.Rows; ++ii)
            {
                for (uint jj = 0; jj < actual.Cols; ++jj)
                {
                    double a = actual[ii, jj];
                     double b = expected[ii, jj];
                    if (Math.Abs(a - b) > epsilon)
                    {
                        Assert.Fail(title, $"{a} != {b} (epsilon: {epsilon}) (coord: {ii},{jj})");
                        return;
                    }
                }
            }
            Assert.Pass(title);
        }
    }
}
