﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace se_unit_tests.UnitTests
{
    class UT_PID : UT_Base
    {
        public UT_PID()
        {
            base.Name = "PID";
        }

        public override void Run()
        {

            // Test proportional
            {
                var pid = new PID(2, 0, 0, 0.1);
                Assert.IsEqual("Proportional works", pid.Control(3), 6.0);
            }

            // Test integral
            {
                var pid = new PID(0, 1, 0, 0.1);
                pid.Control(1);
                pid.Control(2);
                pid.Control(3);
                Assert.IsEqual("Integral works", pid.Control(4), 1.0);
            }

            // Test derivative
            {
                var pid = new PID(0, 0, 2, 0.1);
                pid.Control(1);
                Assert.IsEqual("Derivative works", pid.Control(5), 80);
            }

            // Test decaying integral
            {
                var pid = new DecayingIntegralPID(0, 1, 0, 1, 0.5);
                var expected = new double[] { 1, 0.5 ,0.25, 0.125 };
                int i = 0;
                bool matches = true;
                matches &= pid.Control(1) == expected[i++];
                matches &= pid.Control(0) == expected[i++];
                matches &= pid.Control(0) == expected[i++];
                matches &= pid.Control(0) == expected[i++];

                Assert.IsTrue("Decaying integral works", matches);
            }

            // Test clamped integral
            {
                var pid = new ClampedIntegralPID(0, 1, 0, 1, -5, 5);
                var expected = new double[] { 2, 4, 5, 5, 4, -5, 0};
                int i = 0;
                bool matches = true;
                matches &= pid.Control(2) == expected[i++];
                matches &= pid.Control(2) == expected[i++];
                matches &= pid.Control(2) == expected[i++];
                matches &= pid.Control(2) == expected[i++];
                matches &= pid.Control(-1) == expected[i++];
                matches &= pid.Control(-100) == expected[i++];
                matches &= pid.Control(5) == expected[i++];

                Assert.IsTrue("Clamped integral works", matches);
            }

            // Test buffered integral
            {
                var pid = new BufferedIntegralPID(0, 1, 0, 1, 3);
                var expected = new double[] { 1, 3, 6, 9, 12, 15 };
                int i = 0;
                bool matches = true;
                matches &= pid.Control(1) == expected[i++];
                matches &= pid.Control(2) == expected[i++];
                matches &= pid.Control(3) == expected[i++];
                matches &= pid.Control(4) == expected[i++];
                matches &= pid.Control(5) == expected[i++];
                matches &= pid.Control(6) == expected[i++];

                Assert.IsTrue("Buffered integral works", matches);
            }

        }

        public void AssertIsExpected<T>(OrderedCircularBuffer<T> circularBuffer, T[] expectedArr, string message)
        {
            bool failed = false;
            for (int i = 0; i < expectedArr.Length; ++i)
            {
                T val = circularBuffer[i];
                failed |= val.Equals(expectedArr[i]) == false;
            }
            Assert.IsFalse(message, failed);
        }
    }
}
