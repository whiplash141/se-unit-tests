﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace se_unit_tests.UnitTests
{
    // Class for simply executing child unit tests
    public class UnitTestMain
    {
        readonly List<UT_Base> _unitTests = new List<UT_Base>()
        {
            new UT_VectorMath(),
            new UT_GetRotationAngles(),
            new UT_VectorFromAzimuthAndElevation(),
            new UT_Scheduler(),
            new UT_CircularBuffer(),
            new UT_OrderedCircularBuffer(),
            new UT_KalmanFilter(),
            new UT_CompositeBoundingSphere(),
            //new UT_TaylorSeriesExpansions(),
            new UT_PID(),
            new UT_SwapList(),
            new UT_Scheduler(),
            new UT_StateMachine(),
            //new UT_MathMatrix(),
            new UT_DataReaderAndWriter(),
            new UT_ComplexNumber(),
            new UT_Quadratic(),
            new UT_Cubic(),
            new UT_Quartic(),
        };

        public void RunTests()
        {
            // Comment this out if you want to see passes
            // Assert.PrintPasses = false;

            Console.WriteLine("=====================================");
            Console.WriteLine("STARTING UNIT TESTS ");
            Console.WriteLine("=====================================");
            bool first = true;
            foreach (var ut in _unitTests)
            {
                if(!first)
                    Console.WriteLine("=====================================");
                Console.WriteLine($"    Testing {ut.Name}:");

                Assert.Reset(ut.Name);
                ut.Run();
                Assert.PrintSummary();

                if (first)
                    first = false;
            }
            Console.WriteLine("=====================================");
            Console.WriteLine("    DONE TESTING ");
            Assert.PrintTotalSummary();
            Console.WriteLine("=====================================");
            Console.WriteLine("\nPress any key to continue...");
            Console.ReadKey();
        }
    }
}
