﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using se_unit_tests.UnitTests.Dummies;

namespace se_unit_tests.UnitTests
{
    class UT_Scheduler : UT_Base
    {
        const long STEP_TICKS = 166666L;
        Program programDummy = new Program();
        Scheduler scheduler;
        ExecutedAction executedActions = ExecutedAction.None;

        [Flags]
        public enum ExecutedAction : long
        {
            None = 0,
            Scheduled1Hz = 1,
            Scheduled3Hz = 1 << 1,
            Scheduled10Hz = 1 << 2,
            Scheduled60Hz = 1 << 3,
            ScheduledNext = 1 << 4,
            Queued0s = 1 << 5,
            Queued0p1s = 1 << 6,
            Queued0p5s = 1 << 7,
        }

        public UT_Scheduler()
        {
            this.Name = "Scheduler";
        }

        public override void Run()
        {

            programDummy.Runtime.TimeSinceLastRun = new TimeSpan(STEP_TICKS);

            TestScheduling();
            TestQueuing();

        }

        void TestScheduling()
        {
            scheduler = new Scheduler(programDummy);

            scheduler.AddScheduledAction(Scheduled60Hz, 60);
            scheduler.AddScheduledAction(Scheduled1Hz, 1);
            scheduler.AddScheduledAction(Scheduled10Hz, 10);
            scheduler.AddScheduledAction(Scheduled3Hz, 3);

            const int ticksToSim = 61;

            ExecutedAction[] expectedExecutions = new ExecutedAction[ticksToSim]
            {
                ExecutedAction.Scheduled60Hz, //  1
                ExecutedAction.Scheduled60Hz, //  2
                ExecutedAction.Scheduled60Hz, //  3
                ExecutedAction.Scheduled60Hz, //  4
                ExecutedAction.Scheduled60Hz, //  5
                ExecutedAction.Scheduled60Hz | ExecutedAction.Scheduled10Hz, //  6
                ExecutedAction.Scheduled60Hz, //  7
                ExecutedAction.Scheduled60Hz, //  8
                ExecutedAction.Scheduled60Hz, //  9
                ExecutedAction.Scheduled60Hz, // 10
                ExecutedAction.Scheduled60Hz, // 11
                ExecutedAction.Scheduled60Hz | ExecutedAction.Scheduled10Hz, // 12
                ExecutedAction.Scheduled60Hz, // 13
                ExecutedAction.Scheduled60Hz, // 14
                ExecutedAction.Scheduled60Hz, // 15
                ExecutedAction.Scheduled60Hz, // 16
                ExecutedAction.Scheduled60Hz, // 17
                ExecutedAction.Scheduled60Hz | ExecutedAction.Scheduled10Hz, // 18
                ExecutedAction.Scheduled60Hz, // 19
                ExecutedAction.Scheduled60Hz | ExecutedAction.Scheduled3Hz, // 20
                ExecutedAction.Scheduled60Hz | ExecutedAction.ScheduledNext, // 21
                ExecutedAction.Scheduled60Hz, // 22
                ExecutedAction.Scheduled60Hz, // 23
                ExecutedAction.Scheduled60Hz | ExecutedAction.Scheduled10Hz, // 24
                ExecutedAction.Scheduled60Hz, // 25
                ExecutedAction.Scheduled60Hz, // 26
                ExecutedAction.Scheduled60Hz, // 27
                ExecutedAction.Scheduled60Hz, // 28
                ExecutedAction.Scheduled60Hz, // 29
                ExecutedAction.Scheduled60Hz | ExecutedAction.Scheduled10Hz, // 30
                ExecutedAction.Scheduled60Hz, // 31
                ExecutedAction.Scheduled60Hz, // 32
                ExecutedAction.Scheduled60Hz, // 33
                ExecutedAction.Scheduled60Hz, // 34
                ExecutedAction.Scheduled60Hz, // 35
                ExecutedAction.Scheduled60Hz | ExecutedAction.Scheduled10Hz, // 36
                ExecutedAction.Scheduled60Hz, // 37
                ExecutedAction.Scheduled60Hz, // 38
                ExecutedAction.Scheduled60Hz, // 39
                ExecutedAction.Scheduled60Hz | ExecutedAction.Scheduled3Hz, // 40
                ExecutedAction.Scheduled60Hz | ExecutedAction.ScheduledNext, // 41
                ExecutedAction.Scheduled60Hz | ExecutedAction.Scheduled10Hz, // 42
                ExecutedAction.Scheduled60Hz, // 43
                ExecutedAction.Scheduled60Hz, // 44
                ExecutedAction.Scheduled60Hz, // 45
                ExecutedAction.Scheduled60Hz, // 46
                ExecutedAction.Scheduled60Hz, // 47
                ExecutedAction.Scheduled60Hz| ExecutedAction.Scheduled10Hz, // 48
                ExecutedAction.Scheduled60Hz, // 49
                ExecutedAction.Scheduled60Hz, // 50
                ExecutedAction.Scheduled60Hz, // 51
                ExecutedAction.Scheduled60Hz, // 52
                ExecutedAction.Scheduled60Hz, // 53
                ExecutedAction.Scheduled60Hz| ExecutedAction.Scheduled10Hz, // 54
                ExecutedAction.Scheduled60Hz, // 55
                ExecutedAction.Scheduled60Hz, // 56
                ExecutedAction.Scheduled60Hz, // 57
                ExecutedAction.Scheduled60Hz, // 58
                ExecutedAction.Scheduled60Hz, // 59
                ExecutedAction.Scheduled60Hz | ExecutedAction.Scheduled10Hz | ExecutedAction.Scheduled3Hz | ExecutedAction.Scheduled1Hz, // 60
                ExecutedAction.Scheduled60Hz | ExecutedAction.ScheduledNext, // 61
            };
            ExecutedAction[] actualExecutions = new ExecutedAction[ticksToSim];

            for (int i = 0; i < ticksToSim; ++i)
            {
                //Console.WriteLine($"=== TICK {i+1} ===");
                executedActions = ExecutedAction.None;
                scheduler.Update();
                //Console.WriteLine(executedActions);
                actualExecutions[i] = executedActions;
            }

            ArrayEquals("Scheduled actions", expectedExecutions, actualExecutions);
        }

        void TestQueuing()
        {
            scheduler = new Scheduler(programDummy);

            scheduler.AddQueuedAction(Queued0s, 0);
            scheduler.AddQueuedAction(Queued0p5s, 0.5);
            scheduler.AddQueuedAction(Queued0p1s, 0.1);

            const int ticksToSim = 40;

            ExecutedAction[] expectedExecutions = new ExecutedAction[ticksToSim]
            {
                ExecutedAction.Queued0s, //  1
                ExecutedAction.None, //  2
                ExecutedAction.None, //  3
                ExecutedAction.None, //  4
                ExecutedAction.None, //  5
                ExecutedAction.None, //  6
                ExecutedAction.None, //  7
                ExecutedAction.None, //  8
                ExecutedAction.None, //  9
                ExecutedAction.None, // 10
                ExecutedAction.None, // 11
                ExecutedAction.None, // 12
                ExecutedAction.None, // 13
                ExecutedAction.None, // 14
                ExecutedAction.None, // 15
                ExecutedAction.None, // 16
                ExecutedAction.None, // 17
                ExecutedAction.None, // 18
                ExecutedAction.None, // 19
                ExecutedAction.None, // 20
                ExecutedAction.None, // 21
                ExecutedAction.None, // 22
                ExecutedAction.None, // 23
                ExecutedAction.None, // 24
                ExecutedAction.None, // 25
                ExecutedAction.None, // 26
                ExecutedAction.None, // 27
                ExecutedAction.None, // 28
                ExecutedAction.None, // 29
                ExecutedAction.None, // 30
                ExecutedAction.Queued0p5s, // 31
                ExecutedAction.None, // 32
                ExecutedAction.None, // 33
                ExecutedAction.None, // 34
                ExecutedAction.None, // 35
                ExecutedAction.None, // 36
                ExecutedAction.Queued0p1s, // 37
                ExecutedAction.None, // 38
                ExecutedAction.None, // 39
                ExecutedAction.None, // 40
            };
            ExecutedAction[] actualExecutions = new ExecutedAction[ticksToSim];

            for (int i = 0; i < ticksToSim; ++i)
            {
                //Console.WriteLine($"=== TICK {i+1} ===");
                executedActions = ExecutedAction.None;
                scheduler.Update();
                //Console.WriteLine(executedActions);
                actualExecutions[i] = executedActions;
            }

            ArrayEquals("Queued actions", expectedExecutions, actualExecutions);
        }

        void Queued0s()
        {
            executedActions |= ExecutedAction.Queued0s;
        }

        void Queued0p1s()
        {
            executedActions |= ExecutedAction.Queued0p1s;
        }

        void Queued0p5s()
        {
            executedActions |= ExecutedAction.Queued0p5s;
        }

        void Scheduled60Hz()
        {
            executedActions |= ExecutedAction.Scheduled60Hz;
        }

        void Scheduled1Hz()
        {
            executedActions |= ExecutedAction.Scheduled1Hz;
        }

        void Scheduled10Hz()
        {
            executedActions |= ExecutedAction.Scheduled10Hz;
        }

        void Scheduled3Hz()
        {
            scheduler.AddScheduledAction(ScheduledNextTick, 60, true);
            executedActions |= ExecutedAction.Scheduled3Hz;
        }

        void ScheduledNextTick()
        {
            executedActions |= ExecutedAction.ScheduledNext;
        }

        public static bool ArrayEquals(string title, ExecutedAction[] a, ExecutedAction[] b)
        {
            if (a.Length != b.Length)
            {
                return Assert.Fail(title, $"Array lengths do not match! ({a.Length} != {b.Length})");
            }

            for (int i = 0; i < a.Length; ++i)
            {
                if (a[i] != b[i])
                {
                    return Assert.Fail(title, $"{a[i]} != {b[i]} (idx: {i})");
                }
            }
            return Assert.Pass(title);
        }
    }
}
