﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using NAssert = NUnit.Framework.Assert;

namespace se_unit_tests.UnitTests
{
    [TestFixture]
    class CircularBufferTest
    {
        [Test]
        void TestCapacity()
        {
            bool caught = false;
            try
            {
                CircularBuffer<double> badBuffer = new CircularBuffer<double>(0);
                caught = false;
            }
            catch (Exception)
            {
                caught = true;
            }

            NAssert.IsTrue(caught, "Threw an exception for capacity less than 1");
        }

        [Test]
        void TestReadWrapping()
        {
            CircularBuffer<double> circularBuffer = new CircularBuffer<double>(3);
            circularBuffer.Add(1);
            circularBuffer.Add(2);
            circularBuffer.Add(3);

            double[] expectedArr = new double[9] { 1, 2, 3, 1, 2, 3, 1, 2, 3 };
            AssertIsExpected(circularBuffer, expectedArr, "Buffer properly wraps reads at boundary");
        }

        [Test]
        void TestWriteWrapping()
        {
            CircularBuffer<double> circularBuffer = new CircularBuffer<double>(3);
            circularBuffer.Add(1);
            circularBuffer.Add(2);
            circularBuffer.Add(3);
            circularBuffer.Add(4);
            circularBuffer.Add(5);
            circularBuffer.Add(6);
            circularBuffer.Add(7);

            double[] expectedArr = new double[3] { 7, 5, 6 };
            AssertIsExpected(circularBuffer, expectedArr, "Buffer properly wraps writes at boundary");
        }

        [Test]
        void TestPeek()
        {
            CircularBuffer<double> circularBuffer = new CircularBuffer<double>(3);
            circularBuffer.Add(7);
            circularBuffer.Add(3);
            circularBuffer.Add(11);

            circularBuffer.MoveNext();

            Assert.IsEqual("Peeking does not progress read index", circularBuffer.Peek(), circularBuffer.Peek());
            Assert.IsEqual("Peeking shows correct value", circularBuffer.Peek(), 3);
        }

        public void AssertIsExpected<T>(CircularBuffer<T> circularBuffer, T[] expectedArr, string message)
        {
            bool failed = false;
            for (int i = 0; i < expectedArr.Length; ++i)
            {
                T val = circularBuffer.MoveNext();
                failed |= val.Equals(expectedArr[i]) == false;
            }
            NAssert.IsFalse(failed, message);
        }
    }
}
