﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace se_unit_tests.UnitTests
{
    class UT_CircularBuffer : UT_Base
    {
        public UT_CircularBuffer()
        {
            base.Name = "CircularBuffer";
        }


        public override void Run()
        {

            #region Capacity validation
            {
                bool caught = false;
                try
                {
                    CircularBuffer<double> badBuffer = new CircularBuffer<double>(0);
                    caught = false;
                }
                catch (Exception)
                {
                    caught = true;
                }
                Assert.IsTrue("Threw an exception for capacity less than 1", caught);
            }
            #endregion

            #region Read Wrapping
            {
                CircularBuffer<double> circularBuffer = new CircularBuffer<double>(3);
                circularBuffer.Add(1);
                circularBuffer.Add(2);
                circularBuffer.Add(3);

                double[] expectedArr = new double[9] { 1, 2, 3, 1, 2, 3, 1, 2, 3 };
                AssertIsExpected(circularBuffer, expectedArr, "Buffer properly wraps reads at boundary");
            }
            #endregion

            #region Write Wrapping
            // Expected 3,4,5
            {
                CircularBuffer<double> circularBuffer = new CircularBuffer<double>(3);
                circularBuffer.Add(1);
                circularBuffer.Add(2);
                circularBuffer.Add(3);
                circularBuffer.Add(4);
                circularBuffer.Add(5);
                circularBuffer.Add(6);
                circularBuffer.Add(7);

                double[] expectedArr = new double[3] { 7, 5, 6 };
                AssertIsExpected(circularBuffer, expectedArr, "Buffer properly wraps writes at boundary");
            }
            #endregion

            #region Peek Test
            {
                CircularBuffer<double> circularBuffer = new CircularBuffer<double>(3);
                circularBuffer.Add(7);
                circularBuffer.Add(3);
                circularBuffer.Add(11);

                circularBuffer.MoveNext();

                Assert.IsEqual("Peeking does not progress read index", circularBuffer.Peek(), circularBuffer.Peek());
                Assert.IsEqual("Peeking shows correct value", circularBuffer.Peek(), 3);
            }
            #endregion

        }

        public void AssertIsExpected<T>(CircularBuffer<T> circularBuffer, T[] expectedArr, string message)
        {
            bool failed = false;
            for (int i = 0; i < expectedArr.Length; ++i)
            {
                T val = circularBuffer.MoveNext();
                failed |= val.Equals(expectedArr[i]) == false;
            }
            Assert.IsFalse(message, failed);
        }
    }
}
