﻿using System;
using VRageMath;

namespace se_unit_tests.UnitTests
{
    class UT_GetRotationAngles : UT_Base
    {
        public UT_GetRotationAngles()
        {
            this.Name = "GetRotationAngles";
        }

        void TestAngles(string title, Vector3D front, Vector3D up, MatrixD worldMatrix, double expectedYaw, double expectedPitch, double expectedRoll)
        {
            double pitch, yaw, roll;
            GetRotationAnglesContainer.GetRotationAngles(front, up, worldMatrix, out yaw, out pitch, out roll);

            Assert.IsEqual($"{title} (yaw)", expectedYaw, yaw);
            Assert.IsEqual($"{title} (pitch)", expectedPitch, pitch);
            Assert.IsEqual($"{title} (roll)", expectedRoll, roll);
        }

        public override void Run()
        {

            MatrixD worldMatrix = MatrixD.Identity;

            // Straight forward
            
            TestAngles("Straight forward, no roll", Vector3D.Forward, Vector3D.Up, worldMatrix, 0, 0, 0);
            TestAngles("Straight forward, negative roll", Vector3D.Forward, Vector3D.Left, worldMatrix, 0, 0, -MathHelper.PiOver2);
            TestAngles("Straight forward, positive roll", Vector3D.Forward, Vector3D.Right, worldMatrix, 0, 0, MathHelper.PiOver2);
            TestAngles("Straight forward, upsidedown roll", Vector3D.Forward, Vector3D.Down, worldMatrix, 0, 0, MathHelper.Pi);

            // Straight backward
            TestAngles("Straight backward, no roll", Vector3D.Backward, Vector3D.Up, worldMatrix, MathHelper.Pi, 0, 0);
            TestAngles("Straight backward, negative roll", Vector3D.Backward, Vector3D.Right, worldMatrix, MathHelper.Pi, 0, -MathHelper.PiOver2);
            TestAngles("Straight backward, positive roll", Vector3D.Backward, Vector3D.Left, worldMatrix, MathHelper.Pi, 0, MathHelper.PiOver2);
            TestAngles("Straight forward, upsidedown roll", Vector3D.Backward, Vector3D.Down, worldMatrix, MathHelper.Pi, 0, MathHelper.Pi);

            // Forward down
            TestAngles("Forward down, positive roll", Vector3D.Down, Vector3D.Right, worldMatrix, 0, -MathHelper.PiOver2, MathHelper.PiOver2);
            TestAngles("Forward down, negative roll", Vector3D.Down, Vector3D.Left, worldMatrix, 0, -MathHelper.PiOver2, -MathHelper.PiOver2);
            TestAngles("Forward down, no roll", Vector3D.Down, Vector3D.Forward, worldMatrix, 0, -MathHelper.PiOver2, 0);
            TestAngles("Forward down, upsidedown roll", Vector3D.Down, Vector3D.Backward, worldMatrix, 0, -MathHelper.PiOver2, MathHelper.Pi);

            // Forward up
            TestAngles("Forward up, no roll", Vector3D.Up, Vector3D.Backward, worldMatrix, 0, MathHelper.PiOver2, 0);
            TestAngles("Forward up, positive roll", Vector3D.Up, Vector3D.Right, worldMatrix, 0, MathHelper.PiOver2, MathHelper.PiOver2);
            TestAngles("Forward up, negative roll", Vector3D.Up, Vector3D.Left, worldMatrix, 0, MathHelper.PiOver2, -MathHelper.PiOver2);
            TestAngles("Forward up, upsidedown roll", Vector3D.Up, Vector3D.Forward, worldMatrix, 0, MathHelper.PiOver2, MathHelper.Pi);

            // Forward left
            TestAngles("Forward left, no roll", Vector3D.Left, Vector3D.Up, worldMatrix, -MathHelper.PiOver2, 0, 0);
            TestAngles("Forward left, positive roll", Vector3D.Left, Vector3D.Forward, worldMatrix, -MathHelper.PiOver2, 0, MathHelper.PiOver2);
            TestAngles("Forward left, positive roll", Vector3D.Left, Vector3D.Backward, worldMatrix, -MathHelper.PiOver2, 0, -MathHelper.PiOver2);
            TestAngles("Forward left, upsidedown roll", Vector3D.Left, Vector3D.Down, worldMatrix, -MathHelper.PiOver2, 0, MathHelper.Pi);

            // Forward right
            TestAngles("Forward left, no roll", Vector3D.Right, Vector3D.Up, worldMatrix, MathHelper.PiOver2, 0, 0);
            TestAngles("Forward left, positive roll", Vector3D.Right, Vector3D.Backward, worldMatrix, MathHelper.PiOver2, 0, MathHelper.PiOver2);
            TestAngles("Forward left, negative roll", Vector3D.Right, Vector3D.Forward, worldMatrix, MathHelper.PiOver2, 0, -MathHelper.PiOver2);
            TestAngles("Forward left, upsidedown roll", Vector3D.Right, Vector3D.Down, worldMatrix, MathHelper.PiOver2, 0, MathHelper.Pi);

            // Coincident forward and up
            TestAngles("Forward up, up up", Vector3D.Up, Vector3D.Up, worldMatrix, 0, MathHelper.PiOver2, 0);
            TestAngles("Forward left, up left", Vector3D.Left, Vector3D.Left, worldMatrix, -MathHelper.PiOver2, 0, 0);

        }
    }
}
