﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace se_unit_tests.UnitTests
{
    public class KalmanFilter
    {
        int _dimension = 0;
        public int Dimension
        {
            get
            {
                return _dimension;
            }
            private set
            {
                if (value == _dimension)
                {
                    return;
                }

                if (value < 1)
                {
                    throw new Exception("Kalman filter can not have dimension less than 1");
                }
                _dimension = value;
                CurrentEstimate = new double[_dimension];
                _lastEstimate = new double[_dimension];
            }
        }
        public double[] CurrentEstimate { get; private set; }
        public double SensitivityFactor;
        double[] _lastEstimate;
        bool _firstRun = true;
        
        public KalmanFilter(double sensitvityFactor, int dimension = 1)
        {
            SensitivityFactor = sensitvityFactor;
            Dimension = dimension;
            CurrentEstimate = new double[Dimension];
            _lastEstimate = new double[Dimension];
        }

        public void Reset()
        {
            _firstRun = true;
        }

        public void Reset(int dimension)
        {

            Dimension = dimension;
            Reset();
        }

        public void Update(params double[] measuredValue)
        {
            int lim = Math.Min(measuredValue.Length, Dimension);
            for (int i = 0; i < lim; ++i)
            {
                if (_firstRun)
                {
                    CurrentEstimate[i] = measuredValue[i];
                    continue;
                }
                _lastEstimate[i] = CurrentEstimate[i];
                CurrentEstimate[i] = _lastEstimate[i] + SensitivityFactor * (measuredValue[i] - _lastEstimate[i]);
            }

            if (_firstRun)
            {
                _firstRun = false;
            }
        }
    }
}
