﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace se_unit_tests.UnitTests
{
    class UT_KalmanFilter : UT_Base
    {
        public UT_KalmanFilter()
        {
            base.Name = "KalmanFilter";
        }


        public override void Run()
        {
            {
                double[,] testData = { 
                    { 1, 2, 2, 3, 2, 9, 2, 2 }, 
                    { 9, 2, 7, 8, 6, 4, 7, 5 }
                };
                double[] expected = { 2.1543131, 7.2405833 };
                KalmanFilter kf = new KalmanFilter(0.1, 2);
                for (int i = 0; i < testData.GetLength(1); ++i)
                {
                    double[] input = new double[testData.GetLength(0)];
                    for (int j = 0; j < testData.GetLength(0); ++j)
                    {
                        input[j] = testData[j, i];
                    }
                    kf.Update(input);
                }
                Assert.IsTrue("Kalman filter dimension is 2", 2 == kf.Dimension && 2 == kf.CurrentEstimate.Length);
                Assert.IsEqual("Kalman filter value 0 is expected", expected[0], kf.CurrentEstimate[0]);
                Assert.IsEqual("Kalman filter value 1 is expected", expected[1], kf.CurrentEstimate[1]);

                kf.Reset(1);
                for (int i = 0; i < testData.GetLength(1); ++i)
                {
                    kf.Update(testData[0, i]);
                }
                Assert.IsTrue("Kalman filter dimension is 1 after reset", 1 == kf.Dimension && 1 == kf.CurrentEstimate.Length);
                Assert.IsEqual("Kalman filter value is expected after reset", expected[0], kf.CurrentEstimate[0]);
            }

        }
    }
}
