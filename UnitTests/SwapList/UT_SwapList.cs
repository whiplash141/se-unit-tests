﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using se_unit_tests.UnitTests;

namespace se_unit_tests.UnitTests
{
    class UT_SwapList : UT_Base
    {
        public UT_SwapList()
        {
            this.Name = "SwapList";
        }

        public override void Run()
        {

            var swap = new SwapList<int>(5);
            Assert.IsEqual("Active capacity is correct", swap.Active.Capacity, 5);
            Assert.IsEqual("Inactive capacity is correct", swap.Inactive.Capacity, 5);

            swap.Active.Add(1);
            swap.Active.Add(2);
            swap.Active.Add(3);

            swap.Inactive.Add(4);
            swap.Inactive.Add(5);

            ListDeepEquals("Active list is correct before swap", swap.Active, new List<int>() { 1, 2, 3 });
            ListDeepEquals("Inactive list is correct before swap", swap.Inactive, new List<int>() { 4, 5 });

            swap.Swap();

            swap.Active.Add(7);

            ListDeepEquals("Active list is correct after swap", swap.Active, new List<int>() { 4, 5, 7 });
            ListDeepEquals("Inactive list is correct after swap", swap.Inactive, new List<int>() { 1, 2, 3 });

        }

       
        public static bool ListDeepEquals<T>(string title, List<T> a, List<T> b)
        {
            if (a.Count != b.Count)
            {
                return Assert.Fail(title, $"List lengths do not match! ({a.Count} != {b.Count})");
            }

            for (int i = 0; i < a.Count; ++i)
            {
                if (!a[i].Equals(b[i]))
                {
                    return Assert.Fail(title, $"{a[i]} != {b[i]} (idx: {i})");
                }
            }
            return Assert.Pass(title);
        }
    }
}
