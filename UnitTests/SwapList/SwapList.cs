﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace se_unit_tests.UnitTests
{
    class SwapList<T>
    {
        public List<T> Active
        {
            get
            {
                return _swap ? _list1 : _list2;
            }
        }

        public List<T> Inactive
        {
            get
            {
                return _swap ? _list2 : _list1;
            }
        }

        bool _swap = false;
        readonly List<T> _list1;
        readonly List<T> _list2;

        public SwapList(int capacity = 16)
        {
            _list1 = new List<T>(capacity);
            _list2 = new List<T>(capacity);
        }

        public void Swap()
        {
            _swap = !_swap;
        }
    }
}
