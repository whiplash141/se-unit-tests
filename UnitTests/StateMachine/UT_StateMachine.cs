﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace se_unit_tests.UnitTests
{
    class UT_StateMachine : UT_Base
    {
        public UT_StateMachine()
        {
            base.Name = "StateMachine";
        }

        StateMachineData stateMachineData = new StateMachineData();
        void IncrementDataValue()
        {
            stateMachineData.Value++;
        }

        enum States : long { NoOp, IncrementValue, TransitionToNoOp }

        class NoOpState : IState
        {
            public Enum Id { get; } = States.NoOp;
            public Action OnUpdate { get; } = null;
            public Action OnEnter { get; } = null;
            public Action OnLeave{ get; } = null;

        }

        class TransitionToNoOp : IState
        {
            const int CyclesToDelay = 3;
            int _cycle = 0;
            StateMachine _stateMachine;

            public Enum Id { get; } = States.TransitionToNoOp;
            public Action OnUpdate { get; }
            public Action OnEnter { get; } = null;
            public Action OnLeave { get; } = null;

            public TransitionToNoOp(StateMachine sm)
            {
                OnUpdate = NoOpAfterCycles;
                _stateMachine = sm;
            }

            void NoOpAfterCycles()
            {
                if (++_cycle >= CyclesToDelay)
                {
                    _stateMachine?.SetState(States.NoOp);
                    return;
                }
            }
        }

        public class StateMachineData
        {
            public int Value;
        }

        enum InvalidEnum { Foo }

        public override void Run()
        {

            var stateMachine = new StateMachine();

            stateMachine.AddState(new NoOpState());
            stateMachine.AddState(new State(States.IncrementValue, IncrementDataValue));
            stateMachine.AddState(new TransitionToNoOp(stateMachine));

            bool duplicateStateCaught = false;
            try
            {
                stateMachine.AddState(new NoOpState());
            }
            catch (ArgumentException)
            {
                duplicateStateCaught = true;
            }
            Assert.IsTrue("StateMachine throws exception on duplicate state add", duplicateStateCaught);

            bool didNotInitCaught = false;
            try
            {
                stateMachine.AddState(new NoOpState());
            }
            catch (Exception)
            {
                didNotInitCaught = true;
            }
            Assert.IsTrue("StateMachine throws exception if run before initialization", didNotInitCaught);

            stateMachine.Initialize(States.NoOp);
            Assert.IsEqual("Initial state is NoOp", Convert.ToInt64(stateMachine.StateId), (long)States.NoOp);

            bool noRuntimeStateMachineMods = false;
            try
            {
                stateMachine.AddState(new State(States.NoOp, () => { }));
            }
            catch (InvalidOperationException)
            {
                noRuntimeStateMachineMods = true;
            }
            Assert.IsTrue("StateMachine throws exception when states are added at runtime", noRuntimeStateMachineMods);

            stateMachine.Update();
            Assert.IsEqual("Initial state is NoOp", Convert.ToInt64(stateMachine.StateId), (long)States.NoOp);

            stateMachine.SetState(States.IncrementValue);
            Assert.IsEqual("State changes to IncrementValue on user input", Convert.ToInt64(stateMachine.StateId), (long)States.IncrementValue);
            Assert.IsEqual("StateMachineData Value is 0", stateMachineData.Value, 0);

            stateMachine.Update();
            Assert.IsEqual("StateMachineData Value increments to 1", stateMachineData.Value, 1);

            stateMachine.Update();
            Assert.IsEqual("StateMachineData Value increments to 2", stateMachineData.Value, 2);

            stateMachine.SetState(States.TransitionToNoOp);
            Assert.IsEqual("State changes to TransitionToNoOp on user input", Convert.ToInt64(stateMachine.StateId), (long)States.TransitionToNoOp);

            stateMachine.Update();
            Assert.IsEqual("State remains TransitionToNoOp cycle 1", Convert.ToInt64(stateMachine.StateId), (long)States.TransitionToNoOp);

            stateMachine.Update();
            Assert.IsEqual("State remains TransitionToNoOp cycle 2", Convert.ToInt64(stateMachine.StateId), (long)States.TransitionToNoOp);

            stateMachine.Update();
            Assert.IsEqual("State automatically transitions to NoOp cycle 3", Convert.ToInt64(stateMachine.StateId), (long)States.NoOp);

            Assert.IsFalse("SetState returns false for unregistered enum value", stateMachine.SetState(InvalidEnum.Foo));

        }

    }
}
