﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace se_unit_tests.UnitTests
{


    public class StateMachine
    {
        public Enum StateId
        {
            get
            {
                return State.Id;
            }
        }
        public IState State { get; private set; } = null;

        Dictionary<Enum, IState> _states = new Dictionary<Enum, IState>();
        bool _initialized = false;

        public void AddStates(params IState[] states)
        {
            foreach (IState state in states)
            {
                AddState(state);
            }
        }

        public void AddState(IState state)
        {
            if (_initialized)
            {
                throw new InvalidOperationException("StateMachine.AddState can not be called after initialization");
            }
            bool uniqueState = !_states.ContainsKey(state.Id);
            if (uniqueState)
            {
                _states[state.Id] = state;
            }
            else
            {
                throw new ArgumentException($"Input state does not have a unique id (id: {state.Id})");
            }
        }

        public bool SetState(Enum stateID)
        {
            IState oldState = State;
            IState newState;
            bool validState = _states.TryGetValue(stateID, out newState) && (oldState == null || oldState.Id != newState.Id);
            if (validState)
            {
                oldState?.OnLeave?.Invoke();
                newState?.OnEnter?.Invoke();
                State = newState;
            }
            return validState;
        }

        public void Initialize(Enum stateId)
        {
            _initialized = SetState(stateId);
            if (!_initialized)
            {
                throw new ArgumentException($"stateId {stateId} does not correspond to any registered state");
            }
        }

        public void Update()
        {
            if (!_initialized)
            {
                throw new Exception($"StateMachine has not been initialized");
            }
            State?.OnUpdate?.Invoke();
        }
    }

    public interface IState
    {
        Enum Id { get; }
        Action OnUpdate { get; }
        Action OnEnter { get; }
        Action OnLeave { get; }
    }

    class State : IState
    {
        public Enum Id { get; }
        public Action OnUpdate { get; }
        public Action OnEnter { get; }
        public Action OnLeave { get; }
        public State(Enum id, Action onUpdate = null, Action onEnter = null, Action onLeave = null)
        {
            Id = id;
            OnUpdate = onUpdate;
            OnEnter = onEnter;
            OnLeave = onLeave;
        }
    }

}
