﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VRageMath;

namespace se_unit_tests.UnitTests
{
    public static class Assert
    {
        static int _totalPassedTests;
        static int _totalFailedTests;

        static int _passedTests;
        static int _failedTests;
        static string _name;
        static HashSet<string> _failingTests = new HashSet<string>();

        public static bool PrintPasses = false;

        public static void ResetAll()
        {
            _totalPassedTests = 0;
            _totalFailedTests = 0;
            _passedTests = 0;
            _failedTests = 0;
            _failingTests.Clear();
        }

        public static void Reset(string name)
        {
            _passedTests = 0;
            _failedTests = 0;
            _name = name;
        }

        public static void PrintSummary()
        {
            Console.WriteLine($"SUMMARY | Passed: {_passedTests}, Failed: {_failedTests}");
        }

        public static void PrintTotalSummary()
        {
            Console.WriteLine($"SUMMARY | Total Passed: {_totalPassedTests}, Total Failed: {_totalFailedTests}");
            if (_totalFailedTests > 0)
            {
                foreach (string testName in _failingTests)
                {
                    Console.WriteLine($"   FAIL | {testName}");
                }
            }
        }

        public static bool IsTrue(string title, bool val)
        {
            if (!val)
            {
                return Fail(title, $"Not true");
            }
            return Pass(title);
        }

        public static bool IsFalse(string title, bool val)
        {
            if (val)
            {
                return Fail(title, $"Not false");
            }
            return Pass(title);
        }

        public static bool IsEqual(string title, long a, long b, string endContent = "")
        {
            if (a != b)
            {
                return Fail(title, $"{a} != {b}{endContent}");
            }
            return Pass(title);
        }

        public static bool IsEqual(string title, double a, double b, double epsilon = 1e-6)
        {
            if (Math.Abs(a - b) > epsilon)
            {
                return Fail(title, $"{a} != {b} (epsilon: {epsilon})");
            }
            return Pass(title);
        }

        public static bool IsEqual(string title, Vector3D a, Vector3D b, double epsilon = 1e-6)
        {
            if ((a - b).Length() > epsilon)
            {
                return Fail(title, $"<{a.X:n3}, {a.Y:n3}, {a.Z:n3}> != <{b.X:n3}, {b.Y:n3}, {b.Z:n3}> (epsilon: {epsilon})");
            }
            return Pass(title);
        }

        public static bool IsNotEqual(string title, long a, long b)
        {
            if (a == b)
            {
                return Fail(title, $"{a} == {b}");
            }
            return Pass(title);
        }

        public static bool IsNotEqual(string title, double a, double b, double epsilon = 1e-6)
        {
            if (Math.Abs(a - b) < epsilon)
            {
                return Fail(title, $"{a} == {b} (epsilon: {epsilon})");
            }
            return Pass(title);
        }

        public static bool IsNotEqual(string title, Vector3D a, Vector3D b, double epsilon = 1e-6)
        {
            if ((a - b).Length() < epsilon)
            {
                return Fail(title, $"<{a.X:n3}, {a.Y:n3}, {a.Z:n3}> == <{b.X:n3}, {b.Y:n3}, {b.Z:n3}> (epsilon: {epsilon})");
            }
            return Pass(title);
        }

        public static bool Fail(string title, string reason)
        {
            _failedTests++;
            _totalFailedTests++;
            _failingTests.Add(_name);
            Console.WriteLine($"   FAIL | {title} | Reason: {reason}");
            return false;
        }

        public static bool Pass(string title)
        {
            _passedTests++;
            _totalPassedTests++;
            if (PrintPasses)
                Console.WriteLine($"   PASS | {title}");
            return true;
        }
    }
}
